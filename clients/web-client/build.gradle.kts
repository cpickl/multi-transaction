plugins {
    kotlin("js") version Deps.Versions.Kotlin
    kotlin("plugin.serialization") version Deps.Versions.Kotlin
    id("io.kotest.multiplatform") version Deps.Versions.Kotest
    id("com.github.ben-manes.versions") version Deps.Versions.Versions
}

dependencies {
    implementation(Deps.MultiTx.Server.ApiSdk)
    implementation(Deps.MultiTx.Commons.Date)
    implementation(Deps.MultiTx.Commons.Money)
    implementation(Deps.MultiTx.Commons.KotlinJS)
    implementation(Deps.Datetime)
    implementation(Deps.KotlinJS.React)
    implementation(Deps.KotlinJS.ReactDom)
    implementation(Deps.KotlinJS.Emotion)
    implementation(Deps.Ktor.Client.Core)
    implementation(Deps.Ktor.Client.JS)
    implementation(Deps.Ktor.Client.Logging)
    implementation(Deps.Ktor.Client.ContentNegotation)
    implementation(Deps.Ktor.Serialization)
    implementation(Deps.Serialization)
    implementation(Deps.Koin.Core)
    implementation(Deps.Logging.Mu)
    implementation(Deps.Coroutines.CoreJS)

    testImplementation(Deps.MultiTx.Server.ApiTest)
    testImplementation(Deps.Kotest.FrameworkEngineJS)
    testImplementation(Deps.Kotest.FrameworkApiJS)
    testImplementation(Deps.Kotest.AssertionsJS)
    testImplementation(Deps.Kotest.PropertyJS)
}

kotlin {
    js(IR) {
        binaries.executable()
        browser {
            commonWebpackConfig {
                cssSupport.enabled = true
            }
            testTask {
                debug = true
                useKarma {
                    useChromeHeadlessNoSandbox()
                }
            }
        }
    }
}
