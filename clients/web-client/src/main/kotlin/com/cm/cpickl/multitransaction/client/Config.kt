package com.cm.cpickl.multitransaction.client

import com.cm.cpickl.multitransaction.commons.kotlinjs.ReactEnvironment
import com.cm.cpickl.multitransaction.commons.kotlinjs.currentEnvironment
import mu.KotlinLoggingLevel
import org.w3c.dom.url.URLSearchParams

data class Config(
    val dataSource: DataSource,
    val loggingLevel: KotlinLoggingLevel = KotlinLoggingLevel.TRACE,
) {
    companion object {
        private const val paramDatasource = "datasource"
        private const val paramBaseurl = "baseurl"
        private const val paramDsServerApi = "server"
        private const val paramDsRepository = "inmemory"

        // TODO figure out current URL dynamically/programmatically
        const val defaultServerBaseUrl = "http://localhost:8042"

        fun detect(documentUrl: String) = Config(
            dataSource = dataSourceByUrl(documentUrl) ?: if (currentEnvironment == ReactEnvironment.Production) {
                DataSource.ServerApi(defaultServerBaseUrl)
            } else {
                DataSource.InMemory
            }
        )

        private fun dataSourceByUrl(documentUrl: String): DataSource? {
            val queryParams = if (documentUrl.contains("?")) URLSearchParams(documentUrl.split("?")[1]) else null
            return when (val queryValue = queryParams?.get(paramDatasource)) {
                null -> null
                paramDsServerApi -> DataSource.ServerApi(
                    queryParams.get(paramBaseurl) ?: error("Mandatory query parameter '$paramBaseurl' is missing!")
                )
                paramDsRepository -> DataSource.InMemory
                else -> error("Unhandled '$paramDatasource' query parameter: '$queryValue'! Must be one of: $paramDsServerApi, $paramDsRepository.")
            }
        }
    }
}

sealed interface DataSource {
    object InMemory : DataSource {
        override fun toString() = "InMemory"
    }

    data class ServerApi(val baseUrl: String) : DataSource
}
