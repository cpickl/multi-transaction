package com.cm.cpickl.multitransaction.client.view

import com.cm.cpickl.multitransaction.client.mainCoroutineScope
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryDto
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApi
import kotlinx.coroutines.launch
import mu.KotlinLogging.logger
import react.FC
import react.Props
import react.dom.html.ReactHTML.p
import react.useEffect
import react.useState

private val log = logger("com.cm.cpickl.multitransaction.client.view.RootView")

external interface RootViewProps : Props {
    var transactionApi: TransactionApi
    var selectedTransactionDetail: TransactionDetailDto?
    var selectedTransactionSummary: TransactionSummaryDto?
    var currentPageRequest: PageRequest
    var currentTransactionsPage: PagedResponse<TransactionSummaryDto>
}

fun rootView() = FC<RootViewProps> { props ->
    var selectedTransactionDetail by useState(props.selectedTransactionDetail)
    var selectedTransactionSummary by useState(props.selectedTransactionSummary)
    var currentPageRequest by useState(props.currentPageRequest)
    var currentTransactionsPage by useState(props.currentTransactionsPage)
    log.info { "RootView.create(); selectedTransactionDetail=$selectedTransactionDetail" }

    useEffect(selectedTransactionSummary) {
        log.info { "selectedTransactionSummary changed to: ${selectedTransactionSummary?.id}" }
        selectedTransactionSummary?.also { tx ->
            mainCoroutineScope.launch {
                selectedTransactionDetail = props.transactionApi.findSingle(tx.id)
            }
        }
    }

    useEffect(currentPageRequest) {
        mainCoroutineScope.launch {
            log.info { "Loading next transactions for: $currentPageRequest" }
            currentTransactionsPage = props.transactionApi.findAll(currentPageRequest)
        }
    }

    if (selectedTransactionSummary != null && selectedTransactionDetail == null) {
        p {
            +"Loading ..."
        }
    } else {
        selectedTransactionDetail?.also { txDetail ->
            detailView {
                transaction = txDetail
                onGoBack = OnGoBack {
                    log.debug { "DetailView onGoBack event." }
                    selectedTransactionDetail = null
                    selectedTransactionSummary = null
                }
            }
        } ?: kotlin.run {
            summaryView {
                transactionsPage = currentTransactionsPage
                pageRequest = currentPageRequest
                onPageRequest = OnPageRequest { pageRequest ->
                    currentPageRequest = pageRequest
                }
                onSelectTransaction = OnSelectTransaction { selectedTx, pageRequest, txsPage ->
                    log.debug { "SummaryView onSelectTransaction event." }
                    selectedTransactionSummary = selectedTx
                    currentPageRequest = pageRequest
                    currentTransactionsPage = txsPage
                }
            }
        }
    }
}
