package com.cm.cpickl.multitransaction.client

import kotlinx.coroutines.MainScope

/** Used throughout the application to asynchronously run code. */
val mainCoroutineScope = MainScope()
