package com.cm.cpickl.multitransaction.client.view

import com.cm.cpickl.multitransacction.commons.date.toReadableString
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryDto
import mu.KotlinLogging.logger
import react.FC
import react.Props
import react.dom.html.ReactHTML.a
import react.dom.html.ReactHTML.li
import react.dom.html.ReactHTML.span
import react.dom.html.ReactHTML.ul

private val log = logger("com.cm.cpickl.multitransaction.client.view.TransactionListView")

external interface TransactionListViewProps : Props {
    var transactionsPage: PagedResponse<TransactionSummaryDto>
    var onClickTransaction: OnClickTransaction
}

fun interface OnClickTransaction {
    operator fun invoke(transaction: TransactionSummaryDto)
}

val transactionListView = FC<TransactionListViewProps> { props ->
    ul {
        props.transactionsPage.forEach { transaction ->
            li {
                a {
                    +transaction.id.value.value
                    href = "#"
                    onClick = {
                        log.info { "TransactionListView.transaction click" }
                        props.onClickTransaction(transaction)
                    }
                }
                span {
                    +" - "
                }
                span {
                    +transaction.created.toReadableString()
                }
            }
        }
    }
}
