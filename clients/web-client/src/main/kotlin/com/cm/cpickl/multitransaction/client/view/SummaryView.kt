package com.cm.cpickl.multitransaction.client.view

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryDto
import mu.KotlinLogging.logger
import react.FC
import react.Props
import react.dom.html.ReactHTML.button

private val log = logger("com.cm.cpickl.multitransaction.client.view.SummaryView")

external interface SummaryViewProps : Props {
    var transactionsPage: PagedResponse<TransactionSummaryDto>
    var pageRequest: PageRequest
    var onSelectTransaction: OnSelectTransaction
    var onPageRequest: OnPageRequest
}

fun interface OnSelectTransaction {
    operator fun invoke(
        transaction: TransactionSummaryDto,
        pageRequest: PageRequest,
        transactionsPage: PagedResponse<TransactionSummaryDto>
    )
}

fun interface OnPageRequest {
    operator fun invoke(pageRequest: PageRequest)
}

val summaryView = FC<SummaryViewProps> { props ->
    log.info { "SummaryView.create()" }

    button {
        +"Prev Page"
        disabled = !props.transactionsPage.hasPrevious
        onClick = {
            log.info { "Button prev page clicked." }
            props.onPageRequest(props.pageRequest.previous())
        }
    }
    button {
        +"Next Page"
        disabled = !props.transactionsPage.hasNext
        onClick = {
            log.info { "Button next page clicked." }
            props.onPageRequest(props.pageRequest.next())
        }
    }
    transactionListView {
        transactionsPage = props.transactionsPage
        this.onClickTransaction = OnClickTransaction {
            log.info { "TransactionListView.onClickTransaction invoked." }
            props.onSelectTransaction(it, props.pageRequest, transactionsPage)
        }
    }
}
