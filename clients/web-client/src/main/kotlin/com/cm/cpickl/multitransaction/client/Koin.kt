package com.cm.cpickl.multitransaction.client

import com.cm.cpickl.multitransaction.commons.money.Currency
import com.cm.cpickl.multitransaction.commons.money.Money
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionId
import com.cm.cpickl.multitransaction.server.api.model.Uuid
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApi
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiClient
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiRepository
import kotlinx.datetime.Instant
import kotlinx.datetime.internal.JSJoda.Clock
import mu.KotlinLogging
import org.koin.core.context.startKoin
import org.koin.dsl.bind
import org.koin.dsl.module

private val log = KotlinLogging.logger("com.cm.cpickl.multitransaction.client.Koin")

fun initKoin(config: Config) = startKoin {
    modules(rootModule(config))
}

fun rootModule(config: Config) = module {
    single {
        log.info { "Wiring datasource: ${config.dataSource}" }
        when (config.dataSource) {
            DataSource.InMemory -> {
                TransactionApiRepository(
                    transactionDetails = buildTransactions(),
                    serverBaseUrl = Config.defaultServerBaseUrl,
                )
            }
            is DataSource.ServerApi -> {
                TransactionApiClient(config.dataSource.baseUrl)
            }
        }
    } bind TransactionApi::class
}

private fun buildTransactions() =
    List(21) {
        TransactionDetailDto(
            id = TransactionId(Uuid.random()),
            created = Instant.fromEpochMilliseconds(Clock.systemUTC().millis().toLong()),
            amount = Money(it.toLong(), Currency.random())
        )
    }
