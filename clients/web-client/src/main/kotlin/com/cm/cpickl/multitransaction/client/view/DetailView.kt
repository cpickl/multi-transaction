package com.cm.cpickl.multitransaction.client.view

import com.cm.cpickl.multitransacction.commons.date.toReadableString
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailDto
import mu.KotlinLogging.logger
import react.FC
import react.Props
import react.dom.html.ReactHTML.button
import react.dom.html.ReactHTML.h2
import react.dom.html.ReactHTML.p

private val log = logger("com.cm.cpickl.multitransaction.client.view.DetailView")

fun interface OnGoBack {
    operator fun invoke()
}

external interface DetailViewProps : Props {
    var transaction: TransactionDetailDto
    var onGoBack: OnGoBack
}

val detailView = FC<DetailViewProps> { props ->
    button {
        +"Back"
        onClick = {
            log.debug { "Back button clicked." }
            props.onGoBack()
        }
    }
    h2 {
        +"Transaction Detail"
    }
    p {
        +"ID: ${props.transaction.id}"
    }
    p {
        +"Created: ${props.transaction.created.toReadableString()}"
    }
    p {
        +"Amount: ${props.transaction.amount.toReadableString()}"
    }
}
