import com.cm.cpickl.multitransaction.client.Config
import com.cm.cpickl.multitransaction.client.initKoin
import com.cm.cpickl.multitransaction.client.view.rootView
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.emptyPagedResponse
import kotlinx.browser.document
import mu.KotlinLoggingConfiguration
import react.create
import react.dom.client.createRoot

fun main() {
    val config = Config.detect(document.URL)
    fun main() {
        val config = Config.detect(document.URL)
        KotlinLoggingConfiguration.LOG_LEVEL = config.loggingLevel
        val koin = initKoin(config).koin

        val container = document.getElementById("rootContainer")
            ?: error("Could not find HTML element with ID 'rootContainer'!")
        document.body?.appendChild(container) ?: error("Document body is not available!")

        createRoot(container).render(rootView().create() {
            transactionApi = koin.get()
            selectedTransactionDetail = null
            selectedTransactionSummary = null
            currentPageRequest = PageRequest.default
            currentTransactionsPage = emptyPagedResponse()
        })
    }

    KotlinLoggingConfiguration.LOG_LEVEL = config.loggingLevel
    val koin = initKoin(config).koin

    val container = document.getElementById("rootContainer")
        ?: error("Could not find HTML element with ID 'rootContainer'!")
    document.body?.appendChild(container) ?: error("Document body is not available!")

    createRoot(container).render(rootView().create() {
        transactionApi = koin.get()
        selectedTransactionDetail = null
        selectedTransactionSummary = null
        currentPageRequest = PageRequest.default
        currentTransactionsPage = emptyPagedResponse()
    })
}
