package com.cm.cpickl.multitransaction.client

import com.cm.cpickl.multitransaction.server.api.test.transactionSummaryDto
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.string.shouldNotBeEmpty
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next

class DummyTest : StringSpec({

    val tx = Arb.transactionSummaryDto().next()

    "Demonstrate reusable API model arbs." {
        tx.id.value.value.shouldNotBeEmpty()
    }
})
