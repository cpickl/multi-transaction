package com.cm.cpickl.multitransaction.clients.desktop

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import tornadofx.App
import tornadofx.Stylesheet
import tornadofx.View
import tornadofx.button
import tornadofx.label
import tornadofx.launch
import tornadofx.onLeftClick
import tornadofx.px
import tornadofx.vbox

object DesktopApp {
    @JvmStatic
    fun main(args: Array<String>) {
        println("Start")
        launch<MyApp>(args)
    }
}

class MyApp : App(MyView::class, Styles::class)

val mainScope = CoroutineScope(Dispatchers.Main)

class MyView : View() {
    override val root = vbox {
        label {
            text = "Multi Desktop Transaction"
        }
        val resultLabel = label {
            text = "Result ..."
        }
        button {
            text = "Click me"
            onLeftClick {
                mainScope.launch {
                    println("button clicked")
                    val api = TransactionApiClient("http://localhost:8082")
                    val txs = api.findAll(PageRequest.default)
                    resultLabel.text = "Got ${txs.totalPages} pages."
                }
            }
        }
    }
}

class Styles : Stylesheet() {
    init {
        label {
//            padding = box(10.px)
            fontSize = 20.px
            fontWeight = javafx.scene.text.FontWeight.BOLD
        }
    }
}
