plugins {
    kotlin("jvm")
    id("org.openjfx.javafxplugin") version Deps.Versions.JavaFxPlugin
    id("application")
}
// TODO use Dependencies thingy
dependencies {
    implementation(Deps.TornadoFX)
//    implementation("org.openjfx:javafx-base:20-ea+4")
//    implementation("org.openjfx:javafx-controls:20-ea+4")

    implementation(project(":server:api:sdk"))
    implementation(Deps.Coroutines.JavaFX)
}
// TODO very old kotlin-reflect lib 1.3 in dependencies?! also two times std-lib, jdk7 and jdk8 one?!

javafx {
    version = "19"
    modules = listOf("javafx.controls", "javafx.fxml")
}
