plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":server:api:model"))
    implementation(project(":server:api:test"))
    implementation(project(":commons:wiremock"))
    implementation(project(":commons:pagination:shared"))
    implementation(Deps.Wiremock)
    implementation(Deps.Selenium)
    implementation(Deps.Kotest.Runner)
    implementation(Deps.Kotest.Property)
    implementation(Deps.Kotest.Assertions)
}
