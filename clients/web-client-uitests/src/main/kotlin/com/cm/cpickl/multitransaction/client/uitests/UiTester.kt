package com.cm.cpickl.multitransaction.client.uitests

import org.openqa.selenium.By
import org.openqa.selenium.chrome.ChromeDriver

fun interface UiQuiter {
    operator fun invoke()
}

class UiTester(
    private val startUrl: String
) {
    fun openHome(): HomePage {
        val driver = ChromeDriver()
        driver.get(startUrl)
        return HomePage(driver) {
            driver.quit()
        }
    }

}

class HomePage(private val driver: ChromeDriver, private val quiter: UiQuiter) {

    fun findColumnContent(columnIndex: Int) {
// cm-table-body
        // cm-table-row
        // cm-table-data authorizations-table-A1
        val x = driver.findElement(By.id("authorizations-table-A1"))
        val y = x.findElement(By.className("content")).findElement(By.tagName("span"))
        println("xxx: ${y.text}")
    }

    fun quit() {
        quiter.quit()
    }

    fun bar() {
        // cm-table-header
        // authorizations-table-B
//        val textBox: WebElement = driver.findElement(By.name("my-text"))
//        val submitButton: WebElement = driver.findElement(By.cssSelector("button"))
    }
}

fun jsonListSample(
    date: String = "2022-10-12T13:43:55+02:00",
    txId: String = "tx id",
) = """[
        {
            "createDateTime": "$date",
            "transactionId": "$txId", 
            "merchantName": "some merchant",
            "amount": { "value": 0.42, "currency": "EUR" }, 
            "processorName": "some processor", 
            "acquirerName": "some acquirer", 
            "brand": "BANCONTACT", 
            "authorizationStatus": "UNKNOWN",
            "authorizationId": "00000000-0000-0000-0000-000000000001",
            "authorizationType": "FINAL_AUTHORIZATION", 
            "transactionType": "PURCHASE", 
            "paymentFacilitatorName": "some pfn"
        }
]"""