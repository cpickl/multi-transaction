package com.cm.cpickl.multitransaction.client.uitests

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.commons.wiremock.WiremockKotestListener
import com.cm.cpickl.multitransaction.server.api.test.transactionSummary
import com.cm.cpickl.multitransaction.server.api.test.transactionSummaryResponseDto
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next

class SortingTest : DescribeSpec() {

    private val wiremock = WiremockKotestListener()

    companion object {
        init {
            System.setProperty("webdriver.chrome.driver", "/Applications/chromedriver-107.0.5304.62")
        }
    }


    init {
        extension(wiremock)

        describe("When sort") {
            it("asd") {
                wiremock.transactionSummary(
                    request = PageRequest.default,
                    response = PagedResponse(
                        items = listOf(Arb.transactionSummaryResponseDto().next()),
                        pageNumber = 1,
                        pageSize = 42,
                        totalPages = 1,
                    )
                )

                val tester = UiTester(startUrl = "http://localhost:8081?datasource=server&baseurl=http://localhost:${wiremock.port}")

                val homePage = tester.openHome()
                homePage.findColumnContent(42)
                homePage.quit()
            }
        }
    }
}
