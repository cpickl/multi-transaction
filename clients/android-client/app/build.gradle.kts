plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    compileSdk = 33
    defaultConfig {
        applicationId = "com.cm.cpickl.multitransaction.clients.android"
        minSdk = 28
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        val release by getting {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    packagingOptions {
        // necessary, as we get coroutines two times; the jvm one and the non-mpp one (from the server-api-sdk/ktor)
        resources.excludes.add("META-INF/kotlinx_coroutines_core.version")
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.3.2"
    }
}

dependencies {
    implementation(Deps.Android.Core)
    implementation(Deps.Android.AppCompat)
    implementation(Deps.Android.Material)
    implementation(Deps.Android.ConstraintLayout)
    implementation(Deps.Android.NavigationFragment)
    implementation(Deps.Android.NavigationUI)
    implementation(Deps.MultiTx.Commons.Date)
    implementation(Deps.MultiTx.Server.ApiSdk) {
        exclude(group = "org.jetbrains.kotlinx", module = "kotlinx-coroutines-core")
    }

    val composeBom = platform(Deps.Android.Compose.BOM)
    implementation(composeBom)
    androidTestImplementation(composeBom)
    implementation(Deps.Android.Compose.Material_)
    implementation(Deps.Android.Compose.Material3_)
    implementation(Deps.Android.Compose.ToolingPreview_)
    implementation(Deps.Android.Compose.Tooling_)
    implementation(Deps.Android.Compose.UITest_)
    implementation(Deps.Android.Compose.UITestManifest_)
    implementation(Deps.Android.Compose.Activity)

    testImplementation(Deps.JUnit)
    androidTestImplementation(Deps.Android.JUnitExt)
    androidTestImplementation(Deps.Android.EspressoCore)
}
