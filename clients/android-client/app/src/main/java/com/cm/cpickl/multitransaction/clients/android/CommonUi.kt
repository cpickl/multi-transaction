package com.cm.cpickl.multitransaction.clients.android

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp

@Composable
fun RowScope.TableCell(
    text: String,
    weight: Float
) {
    Text(
        text = text,
        Modifier
//            .border(1.dp, Color.Black)
            .weight(weight)
            .padding(8.dp)
    )
}

@Composable
fun <T> RowScope.TableCellButton(
    text: String,
    weight: Float,
    clickValue: T,
    onClick: (T) -> Unit,
) {
    androidx.compose.material.Button(
        onClick = { onClick(clickValue) },
        modifier = Modifier
//            .border(1.dp, Color.Black)
            .weight(weight)
            .padding(8.dp)
    ) {
        Text(text)
    }
}
