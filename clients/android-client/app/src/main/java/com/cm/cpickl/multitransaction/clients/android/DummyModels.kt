package com.cm.cpickl.multitransaction.clients.android

import com.cm.cpickl.multitransaction.commons.money.Money
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionId
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryDto
import com.cm.cpickl.multitransaction.server.api.model.Uuid
import kotlinx.datetime.Clock

val dummyDetails = List(12) {
    TransactionDetailDto(
        id = TransactionId(Uuid.random()),
        created = Clock.System.now(),
        amount = Money.euroCents(42_00)
    )
}

val dummySummaries = dummyDetails.map {
    TransactionSummaryDto(
        it.id,
        it.created,
        "http://foo/transactions/${it.id}"
    )
}

val dummyPage = PagedResponse(dummySummaries.take(10), 1, 10, 2)
