package com.cm.cpickl.multitransaction.clients.android

import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApi
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

val transactionApi: TransactionApi = TransactionApiRepository(
    transactionDetails = dummyDetails,
    serverBaseUrl = "http://server"
)

val coroutineScope = CoroutineScope(Dispatchers.Main)
