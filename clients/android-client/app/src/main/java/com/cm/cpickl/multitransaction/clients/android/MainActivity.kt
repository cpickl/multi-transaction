package com.cm.cpickl.multitransaction.clients.android

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.Text
import androidx.compose.runtime.rememberCoroutineScope
import com.cm.cpickl.multitransaction.clients.android.databinding.ActivityMainBinding
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiClient
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Text("Loading ...")

            val composableScope = rememberCoroutineScope()
            var currentPageRequest = PageRequest(1, PageRequest.default.pageSize)
            var currentPage = dummyPage
            SummaryView(
                transactionPage = currentPage,
                onPrevPage = {
                    println("prev page")
                },
                onNextPage = {
                    println("next page")
                    currentPageRequest = currentPageRequest.next()
                    composableScope.launch {
                        currentPage = transactionApi.findAll(currentPageRequest)
                        // FIXME now set it
                    }
                },
                onTransactionSummaryClick = {
                    println("summary clicked: $it")
                }
            )
        }
    }
}

class MainActivity_OLD : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->

            coroutineScope.launch {
                val baseUrl = "http://192.168.0.171:8082"
                val api = TransactionApiClient(baseUrl)
                println("Requesting to: $baseUrl")
                try {
                    val txs = api.findAll(PageRequest.default)
                    Snackbar.make(
                        view,
                        "Server found ${txs.totalPages} pages of transactions.",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                } catch (e: Exception) {
                    Toast.makeText(
                        view.context,
                        "Exception while requesting server: ${e.message}",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}
