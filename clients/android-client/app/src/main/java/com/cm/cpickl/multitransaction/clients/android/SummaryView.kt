package com.cm.cpickl.multitransaction.clients.android

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.cm.cpickl.multitransacction.commons.date.toReadableString
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryDto

@Composable
fun SummaryView(
    transactionPage: PagedResponse<TransactionSummaryDto>,
    onPrevPage: OnPrevPage,
    onNextPage: OnNextPage,
    onTransactionSummaryClick: OnTransactionSummaryClick,
//    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    val foo = remember {
        mutableStateOf(true)
    }

    Column {
        Text(text = "Page ${transactionPage.pageNumber} / ${transactionPage.totalPages}")
        NavigationBar(transactionPage, onPrevPage, onNextPage)
        TransactionsTable(
            transactionPage = transactionPage,
            onTransactionSummaryClick = onTransactionSummaryClick,
        )
    }
//    LaunchedEffect(scaffoldState) {
//        val transactionPage = transactionApi.findAll(PageRequest.default)
//        TransactionsTable(transactionPage)
//    }
}

@Preview
@Composable
fun PreviewSummaryView() {
    SummaryView(
        transactionPage = dummyPage,
        onPrevPage = { println("prev page") },
        onNextPage = { println("next page") },
        onTransactionSummaryClick = OnTransactionSummaryClick {
            println("summary clicked: $it")
        },
    )
}

fun interface OnPrevPage {
    operator fun invoke()
}

fun interface OnNextPage {
    operator fun invoke()
}

@Composable
fun NavigationBar(
    transactionPage: PagedResponse<TransactionSummaryDto>,
    onPrevPage: OnPrevPage,
    onNextPage: OnNextPage,
) {
    Row {
        Button(onClick = { onPrevPage() }, enabled = transactionPage.hasPrevious) {
            Text("Prev Page")
        }
        Button(onClick = { onNextPage() }, enabled = transactionPage.hasNext) {
            Text("Next Page")
        }
    }
}


fun interface OnTransactionSummaryClick {
    operator fun invoke(transaction: TransactionSummaryDto)
}

@Composable
fun TransactionsTable(
    transactionPage: PagedResponse<TransactionSummaryDto>,
    onTransactionSummaryClick: OnTransactionSummaryClick,
) {
    val tableData = transactionPage.items
//        it.id.value.value to
    val column1Weight = .3f // 30%
    val column2Weight = .7f // 70%
    // The LazyColumn will be our table. Notice the use of the weights below
    LazyColumn(
        Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        item {
            Row(Modifier.background(Color.Gray)) {
                TableCell(text = "ID", weight = column1Weight)
                TableCell(text = "Created", weight = column2Weight)
            }
        }
        items(tableData) {
            Row(Modifier.fillMaxWidth()) {
                TableCellButton(
                    clickValue = it,
                    text = it.id.value.value,
                    weight = column1Weight,
                    onClick = {
                        onTransactionSummaryClick(it)
                    })
                TableCell(
                    text = it.created.toReadableString(),
                    weight = column2Weight
                )
            }
        }
    }
}
