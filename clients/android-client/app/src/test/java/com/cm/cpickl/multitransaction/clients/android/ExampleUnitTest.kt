package com.cm.cpickl.multitransaction.clients.android

import kotlinx.datetime.Clock
import kotlinx.datetime.toJavaInstant
import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDateTime

// http://d.android.com/tools/testing
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
