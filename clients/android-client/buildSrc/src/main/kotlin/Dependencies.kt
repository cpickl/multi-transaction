@file:Suppress("unused")

import com.cm.cpickl.multitransaction.dependencies.BaseVersions

val Deps = com.cm.cpickl.multitransaction.dependencies.Dependencies(MyVersions)

object MyVersions : BaseVersions() {
    // override val Datetime = "x.y.z"
}
