plugins {
    `kotlin-dsl`
}

dependencies {
    implementation("com.cm.cpickl.multitransaction:dependencies:1.0-SNAPSHOT")
}

repositories {
    mavenLocal()
    mavenCentral()
}
