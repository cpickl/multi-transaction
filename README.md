# multi-transactions

A Kotlin Multi-Platform-Project showcasing the possibilities of having a shared codebase between server and client.
The models along with a SDK are provided by the server, and can be reused as is on the frontend.

## Tech Stack

Technologies being used:

**Backend**

* Ktor Server
* Kotlinx Serialization
* Koin
* Mu Logging
* Wiremock
* Kotest
* Mockk

**Frontend**

* React
* Ktor Client
* Kotlinx Serialization
* Koin
* Kotest
* Mu Logging

## Todos

**High Prio**

* WIP: UI tests with selenium for web-client
* WIP: pagination hat mode fuer query params, to switch to skip/take
* add arrow's Either (ApiClient, indicating error back, instead throwing implicit exceptions... especially PUT
  operation!)
* ! full CRUD+detail operations
* attach sources to JARs
* reusable CM lib (react components with logic)
* filter/sort support
* API error handling (shared model)
* test fixtures (https://docs.gradle.org/current/userguide/java_testing.html#sec:java_test_fixtures)
* shared dependencies via gradle catalog (publish to maven local)
* iOS app for iphone using server-api-sdk
* in commons-pagination/jsMain, react components hinzu

* secured endpoints (JWT/oauth)
* add ios client
* bdd gherkin ui tests schreiben, per ui dann tech spezifische driver (selenium, uirobots,...)
* add another service B, using service A, to showcase reusability approach (model, client, testmodels, test stubs,...)

**Low Prio**

* low profile UI tests (low profile with react&dom)
* Dockerize
* Docker Compose for client
* Expose
* Liquibase/Flyway (the XML one)
* minor: reference project deps with compile time constant (instead strings)
* GitLab build (deploy to rancher/kubernetes)
* BDD on top of selenium