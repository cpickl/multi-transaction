allprojects {
    group = "com.cm.cpickl.multitransaction.server.api"
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.multiplatform")
}
