plugins {
    kotlin("plugin.serialization")
}

kotlin {
    jvm()
    js(IR) {
        browser()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":commons:money"))
                api(Deps.Datetime)
                implementation(Deps.Serialization)
            }
        }
    }
}
