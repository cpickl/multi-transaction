package com.cm.cpickl.multitransaction.server.api.model

actual value class TransactionIdDto(val value: Uuid) {
    actual companion object;
    actual override fun toString() = value.value
}

actual value class Uuid(val value: String) {
    actual companion object {
        private val pattern =
            Regex("""^[\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}${'$'}""")
        private val chars = "abcdef0123456789".toCharArray()
        actual fun random(): Uuid =
            Uuid(listOf(8, 4, 4, 4, 12).joinToString("-") { length ->
                (1..length).map { chars.random() }.joinToString("")
            })
    }

    init {
        require(pattern.matches(value)) { "Passed a non-valid UUID: '$value'!" }
    }
}
