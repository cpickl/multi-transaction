package com.cm.cpickl.multitransaction.server.api.model

import java.util.UUID

@JvmInline
actual value class TransactionIdDto(val value: Uuid) {
    actual companion object;
    actual override fun toString() = value.value
}

@JvmInline
actual value class Uuid(val value: String) {
    actual companion object {
        fun fromUUID(uuid: UUID) =
            Uuid(uuid.toString())

        actual fun random(): Uuid =
            fromUUID(UUID.randomUUID())
    }

    init {
        UUID.fromString(value)
    }

    fun toUUID(): UUID = UUID.fromString(value)
}
