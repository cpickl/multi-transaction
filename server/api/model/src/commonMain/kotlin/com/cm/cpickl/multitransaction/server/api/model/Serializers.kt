package com.cm.cpickl.multitransaction.server.api.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object TransactionIdSerializer : KSerializer<TransactionIdDto> {
    override val descriptor = PrimitiveSerialDescriptor("AuthorizationId", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: TransactionIdDto) {
        encoder.encodeString(value.value.value)
    }

    override fun deserialize(decoder: Decoder) =
        TransactionIdDto(Uuid(decoder.decodeString()))
}
