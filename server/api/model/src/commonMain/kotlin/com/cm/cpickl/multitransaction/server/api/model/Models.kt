@file:UseSerializers(
    TransactionIdSerializer::class,
)


package com.cm.cpickl.multitransaction.server.api.model

import com.cm.cpickl.multitransaction.commons.money.Money
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class HomeResponseDto(
    val transactionsLink: String,
)

@Serializable
data class TransactionSummaryResponseDto(
    val id: TransactionIdDto,
    val created: Instant,
    val detailLink: String,
)

@Serializable
data class TransactionDetailResponseDto(
    val id: TransactionIdDto,
    val created: Instant,
    val amount: Money,
)

@Serializable
data class TransactionUpdateRequestDto(
    val amount: Money,
)

expect value class TransactionIdDto(val value: Uuid) {
    companion object;
    override fun toString(): String
}

// TODO move Uuid to shared commons model lib
expect value class Uuid(val value: String) {
    companion object {
        fun random(): Uuid
    }
}
