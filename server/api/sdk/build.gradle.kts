kotlin {
    jvm()
    js(IR) {
        browser()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":server:api:model"))
                api(project(":commons:pagination:shared"))
                api(project(":commons:pagination:client"))
                implementation(Deps.Datetime)
                implementation(Deps.Ktor.Client.Core)
                implementation(Deps.Ktor.Client.ContentNegotation)
                implementation(Deps.Ktor.Serialization)
                implementation(Deps.Serialization)
                implementation(Deps.Coroutines.Core)
                implementation(Deps.Logging.Mu)
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation(Deps.Ktor.Client.CIO)
            }
        }
        val jsMain by getting {
            dependencies {
                implementation(Deps.Ktor.Client.JS)
            }
        }
    }
}
