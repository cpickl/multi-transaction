package com.cm.cpickl.multitransaction.server.api.sdk

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.commons.pagination.shared.extractPagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionIdDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionUpdateRequestDto
import mu.KotlinLogging

/**
 * Useful for local development purpose.
 */
@Suppress("MemberVisibilityCanBePrivate")
class TransactionApiRepository(
    transactionDetails: List<TransactionDetailResponseDto>,
    private val serverBaseUrl: String,
) : TransactionApi {

    private val log = KotlinLogging.logger("com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiRepository")
    private val transactionDetailsById = transactionDetails.associateBy { it.id }.toMutableMap()
    private val transactionSummariesById = transactionDetails.map {
        it.toTransactionSummaryResponseDto()
    }.associateBy { it.id }.toMutableMap()

    init {
        log.info { "Created new API repo with ${transactionDetails.size} transactions." }
    }

    override suspend fun findAll(pageRequest: PageRequest): PagedResponse<TransactionSummaryResponseDto> {
        log.debug { "findAll($pageRequest)" }
        return transactionSummariesById.values.toList().extractPagedResponse(pageRequest)
    }

    override suspend fun findSingle(transactionId: TransactionIdDto): TransactionDetailResponseDto? {
        log.debug { "findSingle($transactionId)" }
        return transactionDetailsById[transactionId]
    }

    override suspend fun update(transactionId: TransactionIdDto, request: TransactionUpdateRequestDto) {
        val oldTx = findSingle(transactionId) ?: error("Not found: $transactionId")
        val newTx = request.override(oldTx)
        transactionDetailsById[transactionId] = newTx
    }

    private fun TransactionDetailResponseDto.toTransactionSummaryResponseDto() =
        TransactionSummaryResponseDto(
            id = id,
            created = created,
            detailLink = "$serverBaseUrl/transactions/$id",
        )
}

// TODO test this
private fun TransactionUpdateRequestDto.override(old: TransactionDetailResponseDto) =
    old.copy(
        amount = amount
    )
