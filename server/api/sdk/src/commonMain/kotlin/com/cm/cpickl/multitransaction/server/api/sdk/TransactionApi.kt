package com.cm.cpickl.multitransaction.server.api.sdk

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionIdDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionUpdateRequestDto

interface TransactionApi {
    suspend fun findAll(pageRequest: PageRequest): PagedResponse<TransactionSummaryResponseDto>
    suspend fun findSingle(transactionId: TransactionIdDto): TransactionDetailResponseDto?
    suspend fun update(transactionId: TransactionIdDto, request: TransactionUpdateRequestDto)
}
