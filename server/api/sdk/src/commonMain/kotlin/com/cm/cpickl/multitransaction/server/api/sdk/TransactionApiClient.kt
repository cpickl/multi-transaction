package com.cm.cpickl.multitransaction.server.api.sdk

import com.cm.cpickl.multitransaction.commons.client.add
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionIdDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionUpdateRequestDto
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import mu.KotlinLogging.logger

class TransactionApiClient(
    private val baseUrl: String
) : TransactionApi {

    private val log = logger("com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiClient")

    private val client = HttpClient() {
        install(ContentNegotiation) {
            json(
                Json {
                    encodeDefaults = true
                    isLenient = true
                    allowSpecialFloatingPointValues = true
                    allowStructuredMapKeys = true
                    prettyPrint = true
                    useArrayPolymorphism = false
                })
        }
        expectSuccess = false
    }

    override suspend fun findAll(pageRequest: PageRequest): PagedResponse<TransactionSummaryResponseDto> {
        log.info { "findAll($pageRequest)" }
        val response = client.request("$baseUrl/transactions") {
            method = HttpMethod.Get
            add(pageRequest)
        }
        require(response.status == HttpStatusCode.OK) {
            "Invalid response status code: ${response.status}"
        }
        return response.body()
    }

    override suspend fun findSingle(transactionId: TransactionIdDto): TransactionDetailResponseDto? {
        log.info { "findSingle($transactionId)" }
        val response = client.request("$baseUrl/transactions/$transactionId") {
            method = HttpMethod.Get
        }
        return when (response.status) {
            HttpStatusCode.OK -> response.body()
            HttpStatusCode.NotFound -> null
            else -> throw Exception("Invalid response status code: ${response.status}")
        }
    }

    override suspend fun update(transactionId: TransactionIdDto, request: TransactionUpdateRequestDto) {
        log.info { "update($transactionId, $request)" }
        val response = client.request("$baseUrl/transactions/$transactionId") {
            method = HttpMethod.Put
            setBody(request)
        }
        return when (response.status) {
            HttpStatusCode.OK -> {}
            // TODO handle bad request (via Either)
            HttpStatusCode.NotFound -> throw Exception("Failed to update transaction as it was not found by ID: $transactionId")
            else -> throw Exception("Invalid response status code: ${response.status}")
        }
    }
}
