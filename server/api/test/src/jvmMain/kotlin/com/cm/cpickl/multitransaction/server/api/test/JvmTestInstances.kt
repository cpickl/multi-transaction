package com.cm.cpickl.multitransaction.server.api.test

import com.cm.cpickl.multitransaction.server.api.model.Uuid
import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.uuid

actual fun Arb.Companion.uuid(): Arb<Uuid> = arbitrary {
    Uuid.fromUUID(uuid().next())
}
