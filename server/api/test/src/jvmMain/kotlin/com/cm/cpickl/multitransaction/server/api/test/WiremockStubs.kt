package com.cm.cpickl.multitransaction.server.api.test

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.commons.wiremock.HttpMethod
import com.cm.cpickl.multitransaction.commons.wiremock.WiremockKotestListener
import com.cm.cpickl.multitransaction.commons.wiremock.jsonContentHeader
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto

fun WiremockKotestListener.transactionSummary(
    request: PageRequest = PageRequest.default,
    response: PagedResponse<TransactionSummaryResponseDto>,
    statusCode: Int = 200,
) {
    stub {
        request {
            method = HttpMethod.Get
            url = "/transactions?page=${request.pageNumber}&size=${request.pageSize}"
        }
        response {
            status = statusCode
            body = response
            jsonContentHeader()
            headers += "Access-Control-Allow-Origin" to "*"
        }
    }
}

// TODO  fun WiremockKotestListener.transactionDetails(
