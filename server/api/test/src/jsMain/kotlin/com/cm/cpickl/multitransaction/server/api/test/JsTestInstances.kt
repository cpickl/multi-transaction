package com.cm.cpickl.multitransaction.server.api.test

import com.cm.cpickl.multitransaction.server.api.model.Uuid
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.hex
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string

actual fun Arb.Companion.uuid(): Arb<Uuid> = arbitrary {
    Uuid(listOf(8, 4, 4, 4, 12).joinToString("-") { length ->
        Arb.string(length, Codepoint.hex()).next()
    })
}
