package com.cm.cpickl.multitransaction.server.api.test

import io.kotest.assertions.json.shouldEqualJson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import kotlinx.datetime.Instant
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

class SerializationTest : StringSpec({
    "TransactionSummaryDto serialized" {
        val dto = Arb.transactionSummaryResponseDto().next()

        val jsonString = Json.encodeToString(dto)

        jsonString shouldEqualJson """
            {
                "id":"${dto.id.value.value}",
                "created":"${dto.created}",
                "detailLink":"${dto.detailLink}"
            }""".trimIndent()
    }

    "TransactionSummaryDto created serialized" {
        val dto = Arb.transactionSummaryResponseDto().next()
            .copy(created = Instant.parse("2022-10-31T18:57:26+02:00"))

        val jsonElement = Json.parseToJsonElement(Json.encodeToString(dto))

        jsonElement.shouldBeInstanceOf<JsonObject>()
            .get("created").shouldBeInstanceOf<JsonPrimitive>()
            .content shouldBe "2022-10-31T16:57:26Z"
    }
})