package com.cm.cpickl.multitransaction.server.api.test

import com.cm.cpickl.multitransaction.commons.money.Currency
import com.cm.cpickl.multitransaction.commons.money.Money
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionIdDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionUpdateRequestDto
import com.cm.cpickl.multitransaction.server.api.model.Uuid
import io.kotest.property.Arb
import io.kotest.property.arbitrary.Codepoint
import io.kotest.property.arbitrary.alphanumeric
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.az
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.long
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.string
import kotlinx.datetime.Instant

expect fun Arb.Companion.uuid(): Arb<Uuid>

fun Arb.Companion.transactionIdDto() = arbitrary {
    TransactionIdDto(uuid().next())
}

fun Arb.Companion.transactionSummaryResponseDto() = arbitrary {
    TransactionSummaryResponseDto(
        id = transactionIdDto().next(),
        detailLink = string().next(),
        created = instant().next(),
    )
}

fun TransactionSummaryResponseDto.Companion.jsonSample() = """
    {
        "id": "${Arb.string(codepoints = Codepoint.alphanumeric()).next()}",
        "created": "${Arb.instant().next()}",
        "detailLink": "${Arb.string(codepoints = Codepoint.alphanumeric()).next()}",
    }
""".trimIndent()

fun Arb.Companion.transactionDetailResponseDto() = arbitrary {
    TransactionDetailResponseDto(
        id = transactionIdDto().next(),
        created = instant().next(),
        amount = Money.euro(1), // TODO use Arb<Money>
    )
}

fun TransactionDetailResponseDto.Companion.jsonSample() = """
    {
        "id": "${Arb.string(codepoints = Codepoint.alphanumeric()).next()}",
        "created": "${Arb.instant().next()}",
    }
""".trimIndent()

fun Arb.Companion.transactionUpdateRequestDto() = arbitrary {
    TransactionUpdateRequestDto(
        amount = Arb.money().next(),
    )
}

// TODO move to shared lib
fun Arb.Companion.money() = arbitrary {
    Money(
        value = long(min = 0).next(),
        currency = currency().next(),
    )
}

fun Arb.Companion.currency() = arbitrary {
    Currency(
        name = string().next(),
        code = string(size = 3, Codepoint.az()).next().uppercase(),
        number = int(min = 100, max = 999).next(),
        precision = int(min = 0, max = 3).next(),
    )
}

fun Arb.Companion.instant() = arbitrary {
    Instant.fromEpochSeconds(long(min = 0L, max = 10_000).next())
}
