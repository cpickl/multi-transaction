plugins {
    id("io.kotest.multiplatform") version Versions.kotest
}

kotlin {
    jvm()
    js(IR) {
        browser()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":server:api:model"))
                api(project(":commons:money"))
                api(Deps.Kotest.Property)
                implementation(Deps.Coroutines.Core)
                implementation(Deps.Datetime)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(Deps.Kotest.FrameworkEngine)
                implementation(Deps.Kotest.FrameworkApi)
                implementation(Deps.Kotest.Assertions)
                implementation(Deps.Kotest.AssertionsLibJson)
                implementation(Deps.Kotest.Property)
                implementation(Deps.Serialization)
            }
        }
        val jvmMain by getting {
            dependencies {
                api(project(":commons:wiremock"))
                api(project(":commons:pagination:shared"))
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(Deps.Kotest.Runner)
            }
        }
    }
}
