plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":server:api:model"))
    implementation(project(":commons:pagination:shared"))
    implementation(project(":commons:pagination:server"))
    implementation(project(":commons:money"))
    implementation(Deps.Ktor.Server.Core)
    implementation(Deps.Ktor.Server.Netty)
    implementation(Deps.Ktor.Server.CORS)
    implementation(Deps.Ktor.Server.ContentNegotation)
    implementation(Deps.Ktor.Serialization)
    implementation(Deps.Koin.Ktor)
    implementation(Deps.Logging.Mu)
    implementation(Deps.Datetime)
    runtimeOnly(Deps.Logging.Logback)

    testImplementation(project(":server:api:sdk"))
    testImplementation(project(":server:api:test"))
    testImplementation(project(":commons:pagination:test"))
    testImplementation(project(":commons:wiremock"))
    testImplementation(Deps.Ktor.Client.CIO)
    testImplementation(Deps.Ktor.Client.ContentNegotation)
    testImplementation(Deps.Ktor.Server.Test) {
        exclude(group = "junit", module = "junit")
        exclude(group = "org.jetbrains.kotlin", module = "kotlin-test-junit")
    }
    testImplementation(Deps.Kotest.Runner) {
        exclude(group = "org.junit.jupiter", module = "junit-jupiter-api")
    }
    testImplementation(Deps.Kotest.Assertions)
    testImplementation(Deps.Kotest.AssertionsLibJson)
    testImplementation(Deps.Kotest.Property)
    testImplementation(Deps.Mockk)
}
