package com.cm.cpickl.multitransaction.server.app

import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.uuid

fun Arb.Companion.transactionId() = arbitrary {
    TransactionId(uuid().next())
}
