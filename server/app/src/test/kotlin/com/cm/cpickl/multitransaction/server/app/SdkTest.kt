package com.cm.cpickl.multitransaction.server.app

import com.cm.cpickl.multitransaction.commons.pagination.test.pageRequest
import com.cm.cpickl.multitransaction.commons.pagination.test.pagedResponse
import com.cm.cpickl.multitransaction.commons.wiremock.HttpMethod
import com.cm.cpickl.multitransaction.commons.wiremock.WiremockKotestListener
import com.cm.cpickl.multitransaction.commons.wiremock.jsonContentHeader
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.sdk.TransactionApiClient
import com.cm.cpickl.multitransaction.server.api.test.transactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.test.transactionSummaryResponseDto
import com.github.tomakehurst.wiremock.http.RequestMethod
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldBeSingleton
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.shouldBe
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next

class SdkRealServerTest : StringSpec({

    val pageRequest = Arb.pageRequest().next()
    val summaryDto = Arb.transactionSummaryResponseDto().next()
    val detailDto = Arb.transactionDetailResponseDto().next()

    "Given mocked controller When find all Then return items" {
        withTestServer(testModule = moduleWithMockedTransactionController {
            mockFindAll(listOf(summaryDto))
        }) {
            val transactions = TransactionApiClient(baseUrl).findAll(pageRequest)

            transactions.items.shouldNotBeEmpty()
        }
    }

    "Given mocked controller When find single Then return item" {
        withTestServer(testModule = moduleWithMockedTransactionController {
            mockFindSingle(detailDto.id.toDomain(), detailDto)
        }) {
            val found = TransactionApiClient(baseUrl).findSingle(detailDto.id)

            found shouldBe detailDto
        }
    }
})

class SdkWiremockTest : StringSpec({
    val wiremock = WiremockKotestListener()
    extension(wiremock)

    val pageRequest = Arb.pageRequest().next()
    val pagedResponse = Arb.pagedResponse<TransactionSummaryResponseDto>().next()

    "Given wiremock response ok When get transactions Then wiremock was called properly" {
        val requestUrl = "/transactions?pageSize=${pageRequest.pageSize}&pageNumber=${pageRequest.pageNumber}"
        wiremock.stub {
            request {
                url = requestUrl
                method = HttpMethod.Get
            }
            response {
                status = 200
                jsonContentHeader()
                body = pagedResponse
            }
        }

        TransactionApiClient("http://localhost:${wiremock.port}").findAll(pageRequest)

        val loggedRequest = wiremock.allRecordedRequests().shouldBeSingleton().first()
        loggedRequest.url shouldBe requestUrl
        loggedRequest.method shouldBe RequestMethod.GET
        loggedRequest.queryParamsSingled shouldBe mapOf(
            "pageSize" to pageRequest.pageSize.toString(),
            "pageNumber" to pageRequest.pageNumber.toString(),
        )
    }
})
