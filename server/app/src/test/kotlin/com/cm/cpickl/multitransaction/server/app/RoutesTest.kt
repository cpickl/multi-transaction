package com.cm.cpickl.multitransaction.server.app

import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.test.transactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.test.transactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.test.transactionUpdateRequestDto
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.kotest.property.Arb
import io.kotest.property.arbitrary.next
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.put
import io.ktor.client.request.setBody
import io.ktor.http.HttpStatusCode

class RoutesTest : DescribeSpec({

    val summaries = listOf(Arb.transactionSummaryResponseDto().next())
    val detail = Arb.transactionDetailResponseDto().next()
    val anyId = Arb.transactionId().next()
    val invalidUuid = "invalidUuid"
    val updateRequest = Arb.transactionUpdateRequestDto().next()

    describe("GET /transactions") {
        it("Given mocked summaries When get transactions without pagination params Then return ok and summaries") {
            withTestEngine(testModule = moduleWithMockedTransactionController {
                mockFindAll(summaries)
            }) {
                val response = get("/transactions")

                response.status shouldBe HttpStatusCode.OK
                response.body<PagedResponse<TransactionSummaryResponseDto>>().items shouldContainExactly summaries
            }
        }
    }

    describe("GET /transactions/{id}") {
        it("Given mocked detail When get transaction Then return ok and detail") {
            withTestEngine(testModule = moduleWithMockedTransactionController {
                mockFindSingle(detail.id.toDomain(), detail)
            }) {
                val response = get("/transactions/${detail.id}")

                response.status shouldBe HttpStatusCode.OK
                response.body<TransactionDetailResponseDto>() shouldBe detail
            }
        }

        it("Given tx not found When get transaction Then return not found") {
            withTestEngine(testModule = moduleWithMockedTransactionController {
                mockFindSingle(anyId, null)
            }) {
                val response = get("/transactions/$anyId")

                response.status shouldBe HttpStatusCode.NotFound
            }
        }

        it("When get transaction with invalid ID Then return bad request status") {
            withTestEngine {
                val response = get("/transactions/$invalidUuid")

                response.status shouldBe HttpStatusCode.BadRequest
                // TODO response body should be API error, assert message containing the invalidUuid
            }
        }
    }

    describe("PUT /transactions/{id}") {
        it("When update transaction Then mock called") {
            withTestEngine(testModule = moduleWithMockedTransactionController {
                mockUpdate(detail.id.toDomain(), updateRequest)
            }) {

                val response = put("/transactions/${detail.id}") {
                    jsonContentHeader()
                    setBody(updateRequest)
                }

                // FIXME test fails! :(
                response.status shouldBe HttpStatusCode.OK
            }
        }
    }
})

// TODO move to commons ktor client lib
fun HttpRequestBuilder.jsonContentHeader() {
    header("Content-Type", "application/json")
}

