package com.cm.cpickl.multitransaction.server.app

import com.cm.cpickl.multitransaction.commons.money.Money
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionIdDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionUpdateRequestDto
import com.github.tomakehurst.wiremock.verification.LoggedRequest
import io.ktor.client.HttpClient
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.testing.testApplication
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.serialization.json.Json
import org.koin.dsl.ModuleDeclaration
import org.koin.dsl.bind
import org.koin.dsl.module

fun withTestEngine(
    testModule: ModuleDeclaration = {},
    testCode: suspend HttpClient.() -> Unit
) {
    testApplication {
        application {
            startKtorApp(additonalModules = listOf(module(createdAtStart = true, testModule)))
        }

        val client = createClient {
            install(ContentNegotiation) {
                json(Json {
                    encodeDefaults = true
                    isLenient = false
                    allowStructuredMapKeys = true
                    useArrayPolymorphism = false
                })
            }
        }
        client.testCode()
    }
}

private const val testServerPort = 8088
suspend fun withTestServer(
    testModule: ModuleDeclaration = {},
    testCode: suspend ServerContext.() -> Unit
) {
    val server = embeddedServer(Netty, port = testServerPort) {
        startKtorApp(additonalModules = listOf(module(createdAtStart = true, testModule)))
    }
    server.start(wait = false)
    testCode(ServerContext("http://localhost:$testServerPort"))
    server.stop()
}

class ServerContext(
    val baseUrl: String
)

val LoggedRequest.queryParamsSingled: Map<String, String>
    get() = queryParams.entries.associate { it.value.key() to it.value.firstValue() }

fun moduleWithMockedTransactionController(
    mockInit: TransactionController.() -> Unit = {}
): ModuleDeclaration = {
    single { mockk(block = mockInit) } bind TransactionController::class
}

fun TransactionController.mockFindAll(dtos: List<TransactionSummaryResponseDto>) {
    coEvery { findAll(any()) } returns PagedResponse(dtos, 1, 1, 1)
}

fun TransactionController.mockFindSingle(id: TransactionId, dto: TransactionDetailResponseDto?) {
    coEvery { findSingle(id) } returns dto
}

fun TransactionController.mockUpdate(
    id: TransactionId, request: TransactionUpdateRequestDto,
    thrownException: Exception? = null
) {
    coEvery { update(id, request) }.also {
        if (thrownException != null) {
            it.throws(thrownException)
        }
    }
}

fun TransactionIdDto.toDomain() = TransactionId(value.toUUID())

operator fun Money.plus(value: Int) = Money(this.value + value, currency)
