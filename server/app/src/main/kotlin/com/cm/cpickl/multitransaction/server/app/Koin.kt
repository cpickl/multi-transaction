package com.cm.cpickl.multitransaction.server.app

import org.koin.dsl.bind
import org.koin.dsl.module

val koinModule = module {
    single { InMemoryTransactionRepository() } bind TransactionRepository::class
    single {
        TransactionControllerImpl(
            transactionRepository = get(),
            serverBaseUrl = serverBaseUrl
        )
    } bind TransactionController::class
    single { HomeControllerImpl(serverBaseUrl = serverBaseUrl) } bind HomeController::class

}
