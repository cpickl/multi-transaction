package com.cm.cpickl.multitransaction.server.app

import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation
import io.ktor.server.plugins.cors.routing.CORS
import kotlinx.serialization.json.Json
import mu.KotlinLogging.logger
import org.koin.core.module.Module
import org.koin.ktor.plugin.Koin


val config = Config(serverPort = 8082)

// TODO how to read the server base URL dynamically at runtime?
val serverBaseUrl = "http://localhost:${config.serverPort}"

object ServerApp {

    private val log = logger {}

    @JvmStatic
    fun main(args: Array<String>) {
        log.info { "Starting up server on port ${config.serverPort}..." }
        embeddedServer(Netty, port = config.serverPort) {
            startKtorApp()
        }.start(wait = true)
    }
}

fun Application.startKtorApp(additonalModules: List<Module> = emptyList()) {
    install(Koin) {
        modules(mutableListOf(koinModule).plus(additonalModules))
    }
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
        })
    }
    install(CORS) {
        allowHost("*")
    }
    installRouting()
}
