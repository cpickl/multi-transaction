package com.cm.cpickl.multitransaction.server.app

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.server.api.model.HomeResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionDetailResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionIdDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionSummaryResponseDto
import com.cm.cpickl.multitransaction.server.api.model.TransactionUpdateRequestDto
import com.cm.cpickl.multitransaction.server.api.model.Uuid

interface HomeController {
    fun home(): HomeResponseDto
}

class HomeControllerImpl(
    private val serverBaseUrl: String,
) : HomeController {
    override fun home() = HomeResponseDto(
        transactionsLink = "$serverBaseUrl/transactions"
    )
}

interface TransactionController {
    fun findAll(pageRequest: PageRequest): PagedResponse<TransactionSummaryResponseDto>
    fun findSingle(id: TransactionId): TransactionDetailResponseDto?
    fun update(id: TransactionId, request: TransactionUpdateRequestDto)
}

class TransactionControllerImpl(
    private val transactionRepository: TransactionRepository,
    private val serverBaseUrl: String,
) : TransactionController {

    override fun findAll(pageRequest: PageRequest): PagedResponse<TransactionSummaryResponseDto> =
        transactionRepository.findAll(pageRequest).map { it.toTransactionSummaryDto() }

    override fun findSingle(id: TransactionId): TransactionDetailResponseDto? =
        transactionRepository.findSingle(id)?.toTransactionDetailDto()

    override fun update(id: TransactionId, request: TransactionUpdateRequestDto) {
        transactionRepository.update(TransactionUpdateRequest(id, request.amount))
    }


    private fun Transaction.toTransactionSummaryDto() = TransactionSummaryResponseDto(
        id = id.toDto(),
        created = created.toKotlinxInstant(),
        detailLink = "$serverBaseUrl/transactions/$id",
    )

    private fun Transaction.toTransactionDetailDto() = TransactionDetailResponseDto(
        id = id.toDto(),
        created = created.toKotlinxInstant(),
        amount = amount,
    )
}

private fun TransactionId.toDto() = TransactionIdDto(Uuid.fromUUID(value))
