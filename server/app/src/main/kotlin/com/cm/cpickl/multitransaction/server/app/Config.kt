package com.cm.cpickl.multitransaction.server.app

data class Config(
    val serverPort: Int
)
