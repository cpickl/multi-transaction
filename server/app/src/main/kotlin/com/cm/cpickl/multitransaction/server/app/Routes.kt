package com.cm.cpickl.multitransaction.server.app

import com.cm.cpickl.multitransaction.commons.pagination.server.readPageRequest
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.put
import io.ktor.server.routing.routing
import mu.KotlinLogging.logger
import org.koin.ktor.ext.inject

private val log = logger {}

fun Application.installRouting() {
    val homeController by inject<HomeController>()
    val transactionController by inject<TransactionController>()

    routing {
        get("/") {
            call.respond(homeController.home())
        }
        get("/transactions") {
            val pageRequest = call.readPageRequest()
            log.info { "GET /transactions ($pageRequest)" }
            call.respond(transactionController.findAll(pageRequest))
        }
        get("/transactions/{id}") {
            val id = call.paramAsUuid("id")
            log.info { "GET /transactions/$id" }
            val found = transactionController.findSingle(TransactionId(id))
            if (found != null) {
                call.respond(found)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        put("/transactions/{id}") {
            val id = call.paramAsUuid("id")
            log.info { "GET /transactions/$id" }
        }
    }
}
