package com.cm.cpickl.multitransaction.server.app

import kotlinx.datetime.Instant
import java.time.ZonedDateTime
import java.util.UUID

fun ZonedDateTime.toKotlinxInstant(): Instant =
    Instant.fromEpochSeconds(toEpochSecond())

fun UUID(i: Int): UUID =
    UUID.fromString("00000000-0000-0000-0000-${(i + 1).toString().padStart(12, '0')}")
