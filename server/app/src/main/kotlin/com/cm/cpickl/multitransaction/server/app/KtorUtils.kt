package com.cm.cpickl.multitransaction.server.app

import io.ktor.server.application.ApplicationCall
import io.ktor.server.plugins.BadRequestException
import io.ktor.server.request.uri
import java.util.UUID

fun ApplicationCall.paramAsString(pathParam: String): String =
    parameters[pathParam] ?: throw Exception("Missing path parameter '$pathParam' for URL: ${request.uri}")


fun ApplicationCall.paramAsUuid(pathParam: String): UUID =
    paramAsString(pathParam).let {
        try {
            UUID.fromString(it)
        } catch (e: Exception) {
            throw BadRequestException("Invalid UUID: '$it'!", e)
        }
    }
