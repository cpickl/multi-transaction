package com.cm.cpickl.multitransaction.server.app

import com.cm.cpickl.multitransaction.commons.money.Money
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import com.cm.cpickl.multitransaction.commons.pagination.shared.extractPagedResponse
import java.time.ZonedDateTime
import java.util.UUID

@JvmInline
value class TransactionId(val value: UUID) {
    override fun toString() = value.toString()
}

data class Transaction(
    val id: TransactionId,
    val created: ZonedDateTime,
    val amount: Money,
)

data class TransactionUpdateRequest(
    val id: TransactionId,
    val amount: Money,
)

interface TransactionRepository {
    fun findAll(pageRequest: PageRequest): PagedResponse<Transaction>
    fun findSingle(transactionId: TransactionId): Transaction?
    fun update(request: TransactionUpdateRequest)
}

class InMemoryTransactionRepository : TransactionRepository {

    private val transactionsById = mutableMapOf<TransactionId, Transaction>()

    init {
        (1..42).forEach {
            val id = TransactionId(UUID(it))
            transactionsById += id to Transaction(
                id = id,
                created = ZonedDateTime.now(),
                amount = Money.euroCents((Math.random() * 10_000_00).toLong()),
            )
        }
    }

    override fun findAll(pageRequest: PageRequest) =
        transactionsById.values.toList().extractPagedResponse(pageRequest)

    override fun findSingle(transactionId: TransactionId): Transaction? =
        transactionsById[transactionId]

    // TODO test this
    override fun update(request: TransactionUpdateRequest) {
        val old = findSingle(request.id) ?: throw Error("Not found: ${request.id}")
        val new = request.override(old)
        transactionsById[request.id] = new
    }
}

private fun TransactionUpdateRequest.override(old: Transaction) =
    old.copy(
        amount = amount
    )
