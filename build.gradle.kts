import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

allprojects {
    group = "com.cm.cpickl.multitransaction"
    version = "1.0-SNAPSHOT"
}

plugins {
    kotlin("jvm") version Deps.Versions.Kotlin apply false
    kotlin("multiplatform") version Deps.Versions.Kotlin apply false
    kotlin("plugin.serialization") version Deps.Versions.Kotlin apply false
    id("com.github.ben-manes.versions") version Deps.Versions.Versions apply false
    id("io.kotest.multiplatform") version Deps.Versions.Kotest apply false
}

subprojects {
    apply<MavenPublishPlugin>()
    apply(plugin = "com.github.ben-manes.versions")

    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions {
            freeCompilerArgs = listOf(
                "-Xjsr305=strict"
            )
            jvmTarget = "11"
        }
    }
    tasks.withType<Test>().configureEach {
        useJUnitPlatform()
    }
}

tasks.withType<DependencyUpdatesTask> {
    val rejectPatterns =
        listOf("Alpha1", "alpha", "beta", "EAP", "RC", "rc-1", "m", "CR1").map { it.toLowerCase() }
    rejectVersionIf {
        val version = candidate.version.toLowerCase()
        rejectPatterns.any { version.endsWith(it) }
    }
}
