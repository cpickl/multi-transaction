object Versions {
    object plugins {
        val versions = "0.42.0"
    }

    object kotlinJs {
        val react = "18.0.0-pre.332-kotlin-1.6.21"
        val emotion = "11.9.0-pre.332-kotlin-1.6.21"
    }

    val coroutines = "1.6.4"
    val datetime = "0.4.0"
    val kotlin = "1.7.20"
    val kotest = "5.5.1"
    val koin = "3.2.2"
    val ktor = "2.1.3"
    val logback = "1.2.11"
    val mockk = "1.12.5"
    val muLogging = "2.1.23"
    val selenium = "4.5.3"
    val serialization = "1.4.1"
    val wiremock = "2.27.2"
}
