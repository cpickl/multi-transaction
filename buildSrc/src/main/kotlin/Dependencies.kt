@file:Suppress("unused")

import com.cm.cpickl.multitransaction.dependencies.BaseVersions

val Deps = com.cm.cpickl.multitransaction.dependencies.Dependencies(MyVersions)

object MyVersions : BaseVersions() {
    // override val Datetime = "x.y.z"
}

object Dependencies {

    object coroutines {
        val core = coroutines("core")
        val coreJs = coroutines("core-js")
        val coreJvm = coroutines("core-jvm")
        val android = coroutines("android")
        val javafx = coroutines("javafx")
        fun coroutines(artifactIdSuffix: String) =
            "org.jetbrains.kotlinx:kotlinx-coroutines-$artifactIdSuffix:${Versions.coroutines}"
    }

    // parse and format only supports ISO-8601
    val datetime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.datetime}"

    object koin {
        val ktor = koin("ktor")
        val core = koin("core")
         fun koin(artifactIdSuffix: String) =
            "io.insert-koin:koin-$artifactIdSuffix:${Versions.koin}"
    }

    object kotest {
        fun kotest(artifactIdSuffix: String) = "io.kotest:kotest-$artifactIdSuffix:${Versions.kotest}"
        val frameworkEngine = kotest("framework-engine")
        val frameworkEngineJs = kotest("framework-engine-js")
        val frameworkApi = kotest("framework-api")
        val frameworkApiJs = kotest("framework-api-js")
        val runner = "io.kotest:kotest-runner-junit5:${Versions.kotest}"
        val assertions = kotest("assertions-core")
        val assertionsJvm = kotest("assertions-core-jvm")
        val assertionsJs = kotest("assertions-core-js")
        val assertionsLibJson = kotest("assertions-json")
        val property = kotest("property")
        val propertyJs = kotest("property-js")
        val propertyJvm = kotest("property-jvm")
    }

    object ktor {
        object server {
            val core = ktor("core")
            val netty = ktor("netty")
            val cors = ktor("cors")
            val logging = ktor("logging")
            val contentNegotation = ktor("content-negotiation")
            val test = ktor("test-host")

             fun ktor(artifactIdSuffix: String) = "io.ktor:ktor-server-$artifactIdSuffix:${Versions.ktor}"
        }
        object client {
            val core = ktor("core")
            val cio = ktor("cio")
            val js = ktor("js")
            val logging = ktor("logging")
            val contentNegotation = ktor("content-negotiation")

             fun ktor(artifactIdSuffix: String) = "io.ktor:ktor-client-$artifactIdSuffix:${Versions.ktor}"
        }
        val serialization = "io.ktor:ktor-serialization-kotlinx-json:${Versions.ktor}"
    }

    object logging {
         fun mu(artifactIdSuffix: String) =
            "io.github.microutils:kotlin-logging$artifactIdSuffix:${Versions.muLogging}"
        val mu = mu("")
        val muJvm = mu("-jvm")
        val muJs = mu("-js")
        val logback = "ch.qos.logback:logback-classic:${Versions.logback}"
    }

    object kotlinJs {
        val react = wrapper("react", Versions.kotlinJs.react)
        val reactDom = wrapper("react-dom", Versions.kotlinJs.react)
        val emotion = wrapper("emotion", Versions.kotlinJs.emotion)
        fun wrapper(artifactIdSuffix: String, version: String) =
            "org.jetbrains.kotlin-wrappers:kotlin-$artifactIdSuffix:$version"
    }

    val mockk = "io.mockk:mockk:${Versions.mockk}"
    val selenium = "org.seleniumhq.selenium:selenium-java:${Versions.selenium}"
    val serialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.serialization}"
    val wiremock = "com.github.tomakehurst:wiremock-standalone:${Versions.wiremock}"
}
