@file:Suppress("PropertyName", "unused", "CanBeParameter", "MemberVisibilityCanBePrivate")

package com.cm.cpickl.multitransaction.dependencies

class Dependencies(val Versions: BaseVersions = BaseVersions()) {
    val Android = AndroidDependencies(Versions)
    val Coroutines = CoroutinesDependencies(Versions)
    val Datetime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.Datetime}"
    val JUnit = "junit:junit:${Versions.JUnit}"
    val Ktor = KtorDependencies(Versions)
    val Koin = KoinDependencies(Versions)
    val Kotest = KotestDependencies(Versions)
    val KotlinJS = KotlinJSDependencies(Versions)
    val Logging = LoggingDependencies(Versions)
    val Mockk = "io.mockk:mockk:${Versions.Mockk}"
    val MultiTx = MultiTxDependencies(Versions)
    val Selenium = "org.seleniumhq.selenium:selenium-java:${Versions.Selenium}"
    val Serialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.Serialization}"
    val TornadoFX = "no.tornado:tornadofx:${Versions.TornadoFX}"
    val Wiremock = "com.github.tomakehurst:wiremock-standalone:${Versions.Wiremock}"
}

class MultiTxDependencies(private val versions: BaseVersions) {
    private fun multiTx(group: String, artifact: String) =
        "com.cm.cpickl.multitransaction.$group:$artifact:${versions.MultiTx}"

    val Server = ServerDependencies()
    val Commons = CommonsDependencies()

    inner class ServerDependencies {
        val ApiModel = multiTx("server.api", "model")
        val ApiSdk = multiTx("server.api", "sdk")
        val ApiTest = multiTx("server.api", "test")
    }

    inner class CommonsDependencies {
        val Date = multiTx("commons", "date")
        val Money = multiTx("commons", "money")
        val KotlinJS = multiTx("commons", "kotlinjs")
        val Wiremock = multiTx("commons", "wiremock")
        val Pagination = PaginationDependencies()

        inner class PaginationDependencies {
            val Client = multiTx("commons.pagination", "client")
            val Server = multiTx("commons.pagination", "server")
            val Shared = multiTx("commons.pagination", "shared")
            val Test = multiTx("commons.pagination", "test")
        }
    }
}

class AndroidDependencies(private val versions: BaseVersions) {
    val AppCompat = "androidx.appcompat:appcompat:1.5.1"
    val Core = "androidx.core:core-ktx:1.9.0"
    val ConstraintLayout = "androidx.constraintlayout:constraintlayout:2.1.4"
    val Compose = ComposeDependencies(versions)
    val EspressoCore = "androidx.test.espresso:espresso-core:${versions.Espresso}"
    val JUnitExt = "androidx.test.ext:junit:${versions.AndroidJUnitExt}"
    val Material = "com.google.android.material:material:1.7.0"
    val NavigationFragment = "androidx.navigation:navigation-fragment-ktx:2.5.3"
    val NavigationUI = "androidx.navigation:navigation-ui-ktx:2.5.3"
}

class ComposeDependencies(private val versions: BaseVersions) {
    val Activity = "androidx.activity:activity-compose:${versions.ComposeActivity}"
    val BOM = "androidx.compose:compose-bom:${versions.Compose}"
    val Material_ = "androidx.compose.material:material"
    val Material3_ = "androidx.compose.material3:material3"
    val ToolingPreview_ = "androidx.compose.ui:ui-tooling-preview"
    val Tooling_ = "androidx.compose.ui:ui-tooling"
    val UITest_ = "androidx.compose.ui:ui-test-junit4"
    val UITestManifest_ = "androidx.compose.ui:ui-test-manifest"
}

class CoroutinesDependencies(private val versions: BaseVersions) {
    private fun coroutines(artifactIdSuffix: String) =
        "org.jetbrains.kotlinx:kotlinx-coroutines-$artifactIdSuffix:${versions.Coroutines}"

    val Core = coroutines("core")
    val CoreJS = coroutines("core-js")
    val CoreJVM = coroutines("core-jvm")
    val Android = coroutines("android")
    val JavaFX = coroutines("javafx")
}

class KtorDependencies(private val versions: BaseVersions) {
    private fun ktor(artifact: String) =
        "io.ktor:ktor-$artifact:${versions.Ktor}"

    val Server = KtorServerDependencies()
    val Client = KtorClientDependencies()
    val Serialization = ktor("serialization-kotlinx-json")

    inner class KtorServerDependencies() {
        val Core = ktor("server-core")
        val Netty = ktor("server-netty")
        val CORS = ktor("server-cors")
        val Logging = ktor("server-logging")
        val ContentNegotation = ktor("server-content-negotiation")
        val Test = ktor("server-test-host")
    }

    inner class KtorClientDependencies() {
        val Core = ktor("client-core")
        val CIO = ktor("client-cio")
        val JS = ktor("client-js")
        val Logging = ktor("client-logging")
        val ContentNegotation = ktor("client-content-negotiation")
    }
}

class KoinDependencies(private val versions: BaseVersions) {
    private fun koin(artifactIdSuffix: String) =
        "io.insert-koin:koin-$artifactIdSuffix:${versions.Koin}"

    val Core = koin("core")
    val Ktor = koin("ktor")
}

class KotestDependencies(private val versions: BaseVersions) {
    private fun kotest(artifactIdSuffix: String) =
        "io.kotest:kotest-$artifactIdSuffix:${versions.Kotest}"

    val FrameworkEngine = kotest("framework-engine")
    val FrameworkEngineJS = kotest("framework-engine-js")
    val FrameworkApi = kotest("framework-api")
    val FrameworkApiJS = kotest("framework-api-js")
    val Runner = kotest("runner-junit5")
    val Assertions = kotest("assertions-core")
    val AssertionsJVM = kotest("assertions-core-jvm")
    val AssertionsJS = kotest("assertions-core-js")
    val AssertionsLibJson = kotest("assertions-json")
    val Property = kotest("property")
    val PropertyJS = kotest("property-js")
    val PropertyJVM = kotest("property-jvm")
}

class LoggingDependencies(private val versions: BaseVersions) {
    private fun mu(artifactIdSuffix: String) =
        "io.github.microutils:kotlin-logging$artifactIdSuffix:${versions.MuLogging}"

    val Mu = mu("")
    val MuJVM = mu("-jvm")
    val MuJS = mu("-js")
    val Logback = "ch.qos.logback:logback-classic:${versions.Logback}"
}

class KotlinJSDependencies(private val versions: BaseVersions) {
    private fun wrapper(artifactIdSuffix: String, version: String) =
        "org.jetbrains.kotlin-wrappers:kotlin-$artifactIdSuffix:$version"

    val React = wrapper("react", versions.React)
    val ReactDom = wrapper("react-dom", versions.React)
    val Emotion = wrapper("emotion", versions.Emotion)
}
