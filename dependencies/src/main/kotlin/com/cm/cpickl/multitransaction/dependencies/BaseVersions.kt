@file:Suppress("PropertyName", "unused")

package com.cm.cpickl.multitransaction.dependencies

open class BaseVersions {
    open val Android = "7.3.1"
    open val AndroidJUnitExt = "1.1.3"
    open val Coroutines = "1.6.4"
    open val Compose = "2022.10.00"
    open val ComposeActivity = "1.6.1"
    open val Datetime = "0.4.0"
    open val Emotion = "11.9.0-pre.332-kotlin-1.6.21"
    open val Espresso = "3.4.0"
    open val JavaFxPlugin = "0.0.13"
    open val JUnit = "4.13.2"
    open val Kotlin = "1.7.20"
    open val Kotest = "5.5.1"
    open val Koin = "3.2.2"
    open val Ktor = "2.1.3"
    open val Logback = "1.2.11"
    open val Mockk = "1.12.5"
    open val MuLogging = "2.1.23"
    open val MultiTx = "1.0-SNAPSHOT"
    open val React = "18.0.0-pre.332-kotlin-1.6.21"
    open val Selenium = "4.5.3"
    open val Serialization = "1.4.1"
    open val TornadoFX = "1.7.20"
    open val Versions = "0.42.0"
    open val Wiremock = "2.27.2"
}
