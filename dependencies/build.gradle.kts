plugins {
    kotlin("jvm") version "1.7.20"
    `java-library`
    `maven-publish`
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "dependencies"
            groupId = "com.cm.cpickl.multitransaction"
            version = "1.0-SNAPSHOT"
            from(components["java"])
        }
    }
}
