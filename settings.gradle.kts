@file:Suppress("UnstableApiUsage")

rootProject.name = "multi-transaction"

pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositories {
        mavenLocal()
        mavenCentral()
    }
}

// include(":dependencies") // nope

include(":commons")
include(":commons:date")
include(":commons:money")
include(":commons:kotlinjs")
include(":commons:wiremock")
include(":commons:pagination")
include(":commons:pagination:shared")
include(":commons:pagination:server")
include(":commons:pagination:client")
include(":commons:pagination:test")

include(":server")
include(":server:api")
include(":server:api:model")
include(":server:api:sdk")
include(":server:api:test")
include(":server:app")

include(":clients")
// include(":clients:web-client") // is a standalone project. kotlinJS can't be a submodule :'-(
// include(":clients:android-client")
include(":clients:web-client-uitests")
include(":clients:desktop-client")

