plugins {
    kotlin("multiplatform")
    id("io.kotest.multiplatform") version Versions.kotest
}

kotlin {
    jvm()
    js(IR) {
        browser()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":commons:pagination:shared"))
                implementation(Deps.Datetime)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(Deps.Kotest.FrameworkEngine)
                implementation(Deps.Kotest.FrameworkApi)
                implementation(Deps.Kotest.Assertions)
                implementation(Deps.Kotest.AssertionsLibJson)
                implementation(Deps.Kotest.Property)
                implementation(Deps.Serialization)
                implementation(Deps.Coroutines.Core)
            }
        }
    }
}
