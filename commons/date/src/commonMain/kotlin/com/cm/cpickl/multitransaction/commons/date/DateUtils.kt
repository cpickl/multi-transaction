package com.cm.cpickl.multitransaction.commons.date

import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

fun Instant.toReadableString(): String {
    val dt = toLocalDateTime(TimeZone.UTC)
    return listOf(dt.year, dt.monthNumber.padZeroes(), dt.dayOfMonth.padZeroes()).joinToString("-") + " " +
            listOf(dt.hour.padZeroes(), dt.minute.padZeroes(), dt.second.padZeroes()).joinToString(":")
}

private fun Int.padZeroes() = if (this < 10) "0$this" else toString()
