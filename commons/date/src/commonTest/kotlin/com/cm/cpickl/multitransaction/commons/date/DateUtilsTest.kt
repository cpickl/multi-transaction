package com.cm.cpickl.multitransaction.commons.date

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlinx.datetime.Instant

class DateUtilsTest : StringSpec({
    "Instant toReadableString" {
        val instant = Instant.parse("2022-02-01T08:07:05+02:00")

        val readable = instant.toReadableString()

        readable shouldBe "2022-02-01 06:07:05"
    }
})
