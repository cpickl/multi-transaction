package com.cm.cpickl.multitransaction.commons.pagination.shared

object Headers {
    // TODO make typesafe and outsource to commons-http lib
    const val pageSize = "pageSize"
    const val pageNumber = "pageNumber"
    const val skip = "skip"
    const val take = "take"
}
