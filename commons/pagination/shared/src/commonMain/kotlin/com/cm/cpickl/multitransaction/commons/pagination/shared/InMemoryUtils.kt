package com.cm.cpickl.multitransaction.commons.pagination.shared

import kotlin.math.ceil

/**
 * Usuable for SDK libraries for the in-memory fake implementation.
 */
fun <T> List<T>.extractPagedResponse(pageRequest: PageRequest) =
    PagedResponse(
        items = drop((pageRequest.pageNumber - 1) * pageRequest.pageSize).take(pageRequest.pageSize),
        totalPages = ceil(size.toDouble() / pageRequest.pageSize).toInt(),
        pageNumber = pageRequest.pageNumber,
        pageSize = pageRequest.pageSize,
    )
