package com.cm.cpickl.multitransaction.commons.pagination.shared

import kotlinx.serialization.Serializable

fun <T> emptyPagedResponse() = PagedResponse.empty<T>()

data class PageRequest(
    val pageNumber: Int,
    val pageSize: Int,
) {
    companion object {
        val default = PageRequest(1, 10)
    }

    init {
        require(pageNumber > 0) { "Page number must be > 0 but was: $pageNumber" }
        require(pageSize > 0) { "Page size must be > 0 but was: $pageSize" }
    }

    fun next() = copy(pageNumber = pageNumber + 1)
    fun previous() = copy(pageNumber = pageNumber - 1)
}

@Serializable
data class PagedResponse<T>(
    val items: List<T>,
    val pageNumber: Int,
    val pageSize: Int,
    val totalPages: Int,
) {
    companion object {
        fun <E> empty() = PagedResponse<E>(
            items = emptyList(),
            pageNumber = 1,
            pageSize = 1,
            totalPages = 1,
        )
    }

    val hasNext = pageNumber < totalPages
    val hasPrevious = pageNumber > 1

    fun <U> map(converter: (T) -> U): PagedResponse<U> =
        PagedResponse(
            items = items.map(converter),
            pageNumber = pageNumber,
            pageSize = pageSize,
            totalPages = totalPages,
        )

    fun forEach(action: (T) -> Unit) {
        items.forEach(action)
    }
}
