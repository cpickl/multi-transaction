package com.cm.cpickl.multitransaction.commons.pagination.test

import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import com.cm.cpickl.multitransaction.commons.pagination.shared.PagedResponse
import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.next

fun Arb.Companion.pageRequest() = arbitrary {
    PageRequest(
        pageNumber = int(min = 1, max = 999).next(),
        pageSize = int(min = 1, max = 999).next(),
    )
}

fun <T> Arb.Companion.pagedResponse(
    itemGenerator: ((Int) -> T)? = null
) = arbitrary {
    val pageNumber = int(min = 1, max = 999).next()
    val items = if (itemGenerator == null) emptyList<T>() else {
        List(int(min = 0, max = 100).next(), itemGenerator)
    }
    PagedResponse(
        items = items,
        pageNumber = pageNumber,
        pageSize = int(min = 1, max = 999).next(),
        totalPages = pageNumber + int(min = 0, max = 999).next(),
    )
}
