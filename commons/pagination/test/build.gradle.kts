plugins {
    kotlin("multiplatform")
}

kotlin {
    jvm()
    js(IR) {
        browser()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":commons:pagination:shared"))
                api(Deps.Kotest.Property)
                implementation(Deps.Coroutines.Core)
            }
        }
    }
}
