package com.cm.cpickl.multitransaction.commons.client

import com.cm.cpickl.multitransaction.commons.pagination.shared.Headers
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.parameter

fun HttpRequestBuilder.add(pageRequest: PageRequest, mode: PaginationMode = PaginationMode.SizeNumber) {
    when (mode) {
        PaginationMode.SizeNumber -> {
            parameter(Headers.pageSize, pageRequest.pageSize)
            parameter(Headers.pageNumber, pageRequest.pageNumber)
        }
        PaginationMode.SkipTake -> {
            // FIXME implement me
//            parameter(Headers.ski, pageRequest.pageSize)
//            parameter(Headers.pageNumber, pageRequest.pageNumber)
        }
    }

}

enum class PaginationMode {
    SizeNumber,
    SkipTake,
}
