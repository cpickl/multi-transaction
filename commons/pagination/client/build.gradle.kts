plugins {
    kotlin("multiplatform")
}

kotlin {
    jvm()
    js(IR) {
        browser()
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":commons:pagination:shared"))
                implementation(Deps.Ktor.Client.Core)
                implementation(Deps.Coroutines.Core)
            }
        }
        val jvmTest by getting {
            dependencies {
                // FIXME write test here
//                implementation(Deps.Kotest.Runner)
//                implementation(Deps.Kotest.FrameworkApi)
//                implementation(Deps.Ktor.Server.Test) {
//                    exclude(group = "junit", module = "junit")
//                    exclude(group = "org.jetbrains.kotlin", module = "kotlin-test-junit")
//                }
//                implementation(Deps.Wiremock)
            }
        }
    }
}
