plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(":commons:pagination:shared"))
    implementation(Deps.Ktor.Server.Core)
}
