package com.cm.cpickl.multitransaction.commons.pagination.server

import com.cm.cpickl.multitransaction.commons.pagination.shared.Headers
import com.cm.cpickl.multitransaction.commons.pagination.shared.PageRequest
import io.ktor.http.Parameters
import io.ktor.server.application.ApplicationCall

fun ApplicationCall.readPageRequest(defaultPageSize: Int = 10) =
    // TODO support PaginationMode
    PageRequest(
        pageNumber = parameters.readIntOrNull(Headers.pageNumber) ?: 1,
        pageSize = parameters.readIntOrNull(Headers.pageSize) ?: defaultPageSize,
    )

// TODO outsource to some commons-http library
fun Parameters.readIntOrNull(queryName: String): Int? {
    val queryValue = get(queryName) ?: return null
    return queryValue.toIntOrNull()
}
