package com.cm.cpickl.multitransaction.commons.money.generator

import com.cm.cpickl.multitransaction.commons.money.Currency

private val validIdMatcher = Regex("[a-zA-Z0-9_]+")
private val eliminateForIdentifier = listOf('(', ')', '’')
private val replaceForIdentifier = mapOf(
    'í' to 'i',
    'ã' to 'a',
    'é' to 'e',
    'ó' to 'o',
    '&' to '_',
    '–' to '_',
    '-' to '_',
)

fun String.toIdentifier(): String {
    var cleaned = this
    eliminateForIdentifier.forEach {
        cleaned = cleaned.replace(it, ' ')
    }
    replaceForIdentifier.forEach {
        cleaned = cleaned.replace(it.key, it.value)
    }
    val words = cleaned.split(" ").map { it.lowercase() }.filter { it.isNotEmpty() }
    if (words.size == 1) return words.first()
    val identifier = words.first() + words.drop(1).joinToString("") { it.first().uppercase() + it.drop(1) }
    require(identifier.matches(validIdMatcher)) { "Identifier contained invalid characters: '$identifier'" }
    return identifier
}

val Currency.identifier get() = name.toIdentifier() + "_" + code
