package com.cm.cpickl.multitransaction.commons.money.generator

import org.w3c.dom.Element
import org.w3c.dom.Node
import java.nio.file.Path
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.io.path.exists

object XmlParser {

    private val currencyNameOverrules = mapOf(
        "THB" to "Thai Baht",
        "PAB" to "Panamanian Balboa",
        "BBD" to "Barbadian Dollar",
        "VED" to "Venezuelan Bolívar Soberano",
        "VES" to "Venezuelan Bolívar Soberano",
        "BOB" to "Bolivian Boliviano",
        "XOF" to "West African CFA Franc",
        "XAF" to "Central African CFA Franc",
        "BAM" to "Bosnia-Herzegovina Convertible Mark",
        "NIO" to "Nicaraguan Córdoba",
        "GMD" to "Gambian Dalasi",
        "MKD" to "Macedonian Denar",
        "STN" to "São Tomé & Príncipe Dobra",
        "VND" to "Vietnamese Dong",
        "HUF" to "Hungarian Forint",
        "HTG" to "Haitian Gourde",
        "PYG" to "Paraguayan Guarani",
        "UAH" to "Ukrainian Hryvnia",
        "PGK" to "Papua New Guinean Kina",
        "HRK" to "Croatian Kuna",
        "AOA" to "Angolan Kwanza",
        "MMK" to "Myanmar Kyat",
        "GEL" to "Georgian Lari",
        "ALL" to "Albanian Lek",
        "HNL" to "Honduran Lempira",
    )

    fun parseXml(xmlFile: Path): List<XmlCurrency> {
        require(xmlFile.exists()) { "${xmlFile.toAbsolutePath()} expected to exist!" }
        val xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile.toFile())
        val entries = xml.getElementsByTagName("CcyNtry")
        val currencies = mutableListOf<XmlCurrency>()
        for (i in 1..entries.length) {
            val entry = entries.item(i) ?: continue
            if (entry.nodeType != Node.ELEMENT_NODE) {
                continue
            }
            val element = entry as Element
            if (element.contentOfTag("CcyNm") == "No universal currency" ||
                element.contentOfTag("CcyMnrUnts") == "N.A."
            ) {
                continue
            }
            currencies += element.parseToXmlCurrency()
        }
        println("Parsed ${currencies.size} currencies from XML.")
        return currencies
    }

    private fun Element.parseToXmlCurrency() =
        XmlCurrency(
            countryName = contentOfTag("CtryNm"),
            currencyName = contentOfTag("CcyNm"),
            currencyCode = contentOfTag("Ccy"),
            currencyNumber = contentOfTag("CcyNbr").toInt(),
            currencyUnits = contentOfTag("CcyMnrUnts").toInt()
        ).let {
            currencyNameOverrules.get(it.currencyCode)?.let { overruleName ->
                it.copy(currencyName = overruleName)
            } ?: it
        }

    private fun Element.contentOfTag(tagName: String) =
        getElementsByTagName(tagName).item(0).textContent.trim()
}

data class XmlCurrency(
    val countryName: String,
    val currencyName: String,
    val currencyCode: String,
    val currencyNumber: Int,
    val currencyUnits: Int,
)
