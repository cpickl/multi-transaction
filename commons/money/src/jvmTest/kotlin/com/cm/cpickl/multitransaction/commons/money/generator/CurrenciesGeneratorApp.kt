package com.cm.cpickl.multitransaction.commons.money.generator

import com.cm.cpickl.multitransaction.commons.money.Currency
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString
import kotlin.io.path.writeText

object CurrenciesGeneratorApp {

    private val xmlFile = Path("commons/money/currencies.xml")
    private val target = Path("commons/money/src/commonMain/kotlin/com/cm/cpickl/multitransaction/commons/money/Currencies.kt")

    @JvmStatic
    fun main(args: Array<String>) {
        val currenciesFromXml = XmlParser.parseXml(xmlFile).distinctBy { it.currencyCode }.map {
            Currency(
                name = it.currencyName, code = it.currencyCode, number = it.currencyNumber, precision = it.currencyUnits
            )
        }
        val allCurrencies = JvmFiller.fillMissing(currenciesFromXml)

        val code = CodeGenerator.generateCode(allCurrencies.sortedBy { it.identifier })

        target.writeText(code)
        println("Successfully written code to: ${target.absolutePathString()}")
    }

}
