package com.cm.cpickl.multitransaction.commons.money.generator

import com.cm.cpickl.multitransaction.commons.money.Currency

object CodeGenerator {

    fun generateCode(currencies: List<Currency>): String {
        //@formatter:off
        return """package com.cm.cpickl.multitransaction.commons.money

@Suppress("MemberVisibilityCanBePrivate")
object Currencies {
    ${currencies.joinToString("\n    ") { it.toCode() }}
    
    val all = listOf(${currencies.joinToString(", ") { it.identifier }})
    val byCode = all.associateBy { it.code }
}
"""
        //@formatter:on
    }

    private fun Currency.toCode() =
        """val $identifier = Currency(name = "$name", code = "$code", number = $number, precision = $precision)"""

}