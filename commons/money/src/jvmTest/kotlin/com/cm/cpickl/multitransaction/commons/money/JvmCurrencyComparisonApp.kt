package com.cm.cpickl.multitransaction.commons.money

import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeSameInstanceAs
import java.util.Locale
import java.util.Currency as JvmCurrency

object JvmCurrencyComparisonApp {

    private val ignoredMppsAsNotInJvm = setOf("SLE", "UYW")

    @JvmStatic
    fun main(args: Array<String>) {
        Locale.setDefault(Locale.ENGLISH)
        compare()
        missing()
    }

    private fun compare() {
        println("COMPARE")
        println()
        println("MPP size: ${Currencies.all.size}") // 167
        println("JVM size: ${JvmCurrency.getAvailableCurrencies().size}") // 229

        Currencies.all.filterNot { ignoredMppsAsNotInJvm.contains(it.code) }.forEach { currency ->
            val jvmCurrency = currency.toJvmCurrency()
            println("check: $currency (symbol: ${jvmCurrency.symbol})")
            val currency2 = jvmCurrency.toMppCurrency()
            currency shouldBeSameInstanceAs currency2

            if (jvmCurrency.displayName != currency.name) {
                println("    Name: [${currency.name}] => JVM: [${jvmCurrency.displayName}]")
            }
            jvmCurrency.currencyCode shouldBe currency.code
            jvmCurrency.numericCode shouldBe currency.number
            jvmCurrency.defaultFractionDigits shouldBe currency.precision
            println()
        }
    }

    private fun missing() {
        println("MISSING")
        println()

        val jvms = JvmCurrency.getAvailableCurrencies().associateBy { it.currencyCode }.toMutableMap()
        Currencies.all.filterNot { ignoredMppsAsNotInJvm.contains(it.code) }.forEach {
            require(jvms.remove(it.code) != null) { "Currency not found in JVM: $it" }
        }

        println("JVM additions (${jvms.size}):")
        jvms.values.forEach {
            println("${it.currencyCode} - ${it.displayName} - ${it.numericCode} - ${it.defaultFractionDigits}")
        }
    }

}
