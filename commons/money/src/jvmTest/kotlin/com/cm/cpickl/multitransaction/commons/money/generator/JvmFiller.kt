package com.cm.cpickl.multitransaction.commons.money.generator

import com.cm.cpickl.multitransaction.commons.money.Currency
import java.util.Locale

object JvmFiller {

    private val ignoredMppsAsNotInJvm = setOf("SLE", "UYW")

    fun fillMissing(currenciesFromXml: List<Currency>): List<Currency> {
        Locale.setDefault(Locale.ENGLISH)

        val jvms = java.util.Currency.getAvailableCurrencies()
            .filter { it.defaultFractionDigits >= 0 } // Gold and stuff has -1 precision
            .associateBy { it.currencyCode }.toMutableMap()
        currenciesFromXml.filterNot { ignoredMppsAsNotInJvm.contains(it.code) }.forEach {
            require(jvms.remove(it.code) != null) { "Currency not found in JVM: $it" }
        }

        println("JVM additions: ${jvms.size}")
        return currenciesFromXml + jvms.values.map {
            Currency(
                name = it.displayName,
                code = it.currencyCode,
                number = it.numericCode,
                precision = it.defaultFractionDigits
            )
        }
    }

}