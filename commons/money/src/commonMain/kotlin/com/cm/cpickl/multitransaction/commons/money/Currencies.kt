package com.cm.cpickl.multitransaction.commons.money

@Suppress("MemberVisibilityCanBePrivate")
object Currencies {
    val afghanAfghani1927_2002_AFA = Currency(
        name = "Afghan Afghani (1927–2002)",
        code = "AFA",
        number = 4,
        precision = 2
    )
    val afghanAfghani_AFN = Currency(name = "Afghan Afghani", code = "AFN", number = 971, precision = 2)
    val albanianLek_ALL = Currency(name = "Albanian Lek", code = "ALL", number = 8, precision = 2)
    val algerianDinar_DZD = Currency(name = "Algerian Dinar", code = "DZD", number = 12, precision = 2)
    val andorranPeseta_ADP = Currency(name = "Andorran Peseta", code = "ADP", number = 20, precision = 0)
    val angolanKwanza_AOA = Currency(name = "Angolan Kwanza", code = "AOA", number = 973, precision = 2)
    val argentinePeso_ARS = Currency(name = "Argentine Peso", code = "ARS", number = 32, precision = 2)
    val armenianDram_AMD = Currency(name = "Armenian Dram", code = "AMD", number = 51, precision = 2)
    val arubanFlorin_AWG = Currency(name = "Aruban Florin", code = "AWG", number = 533, precision = 2)
    val australianDollar_AUD = Currency(name = "Australian Dollar", code = "AUD", number = 36, precision = 2)
    val austrianSchilling_ATS = Currency(name = "Austrian Schilling", code = "ATS", number = 40, precision = 2)
    val aym_AYM = Currency(name = "AYM", code = "AYM", number = 945, precision = 2)
    val azerbaijanManat_AZN = Currency(name = "Azerbaijan Manat", code = "AZN", number = 944, precision = 2)
    val azerbaijaniManat1993_2006_AZM = Currency(
        name = "Azerbaijani Manat (1993–2006)",
        code = "AZM",
        number = 31,
        precision = 2
    )
    val bahamianDollar_BSD = Currency(name = "Bahamian Dollar", code = "BSD", number = 44, precision = 2)
    val bahrainiDinar_BHD = Currency(name = "Bahraini Dinar", code = "BHD", number = 48, precision = 3)
    val barbadianDollar_BBD = Currency(name = "Barbadian Dollar", code = "BBD", number = 52, precision = 2)
    val belarusianRuble1994_1999_BYB = Currency(
        name = "Belarusian Ruble (1994–1999)",
        code = "BYB",
        number = 112,
        precision = 0
    )
    val belarusianRuble2000_2016_BYR = Currency(
        name = "Belarusian Ruble (2000–2016)",
        code = "BYR",
        number = 974,
        precision = 0
    )
    val belarusianRuble_BYN = Currency(name = "Belarusian Ruble", code = "BYN", number = 933, precision = 2)
    val belgianFranc_BEF = Currency(name = "Belgian Franc", code = "BEF", number = 56, precision = 0)
    val belizeDollar_BZD = Currency(name = "Belize Dollar", code = "BZD", number = 84, precision = 2)
    val bermudianDollar_BMD = Currency(name = "Bermudian Dollar", code = "BMD", number = 60, precision = 2)
    val bolivianBoliviano_BOB = Currency(name = "Bolivian Boliviano", code = "BOB", number = 68, precision = 2)
    val bosnia_herzegovinaConvertibleMark_BAM = Currency(
        name = "Bosnia-Herzegovina Convertible Mark",
        code = "BAM",
        number = 977,
        precision = 2
    )
    val brazilianReal_BRL = Currency(name = "Brazilian Real", code = "BRL", number = 986, precision = 2)
    val bruneiDollar_BND = Currency(name = "Brunei Dollar", code = "BND", number = 96, precision = 2)
    val bulgarianHardLev_BGL = Currency(name = "Bulgarian Hard Lev", code = "BGL", number = 100, precision = 2)
    val bulgarianLev_BGN = Currency(name = "Bulgarian Lev", code = "BGN", number = 975, precision = 2)
    val burundiFranc_BIF = Currency(name = "Burundi Franc", code = "BIF", number = 108, precision = 0)
    val caboVerdeEscudo_CVE = Currency(name = "Cabo Verde Escudo", code = "CVE", number = 132, precision = 2)
    val canadianDollar_CAD = Currency(name = "Canadian Dollar", code = "CAD", number = 124, precision = 2)
    val caymanIslandsDollar_KYD = Currency(name = "Cayman Islands Dollar", code = "KYD", number = 136, precision = 2)
    val centralAfricanCfaFranc_XAF = Currency(
        name = "Central African CFA Franc",
        code = "XAF",
        number = 950,
        precision = 0
    )
    val cfpFranc_XPF = Currency(name = "CFP Franc", code = "XPF", number = 953, precision = 0)
    val chileanPeso_CLP = Currency(name = "Chilean Peso", code = "CLP", number = 152, precision = 0)
    val colombianPeso_COP = Currency(name = "Colombian Peso", code = "COP", number = 170, precision = 2)
    val comorianFranc_KMF = Currency(name = "Comorian Franc", code = "KMF", number = 174, precision = 0)
    val congoleseFranc_CDF = Currency(name = "Congolese Franc", code = "CDF", number = 976, precision = 2)
    val costaRicanColon_CRC = Currency(name = "Costa Rican Colon", code = "CRC", number = 188, precision = 2)
    val croatianKuna_HRK = Currency(name = "Croatian Kuna", code = "HRK", number = 191, precision = 2)
    val cubanPeso_CUP = Currency(name = "Cuban Peso", code = "CUP", number = 192, precision = 2)
    val cypriotPound_CYP = Currency(name = "Cypriot Pound", code = "CYP", number = 196, precision = 2)
    val czechKoruna_CZK = Currency(name = "Czech Koruna", code = "CZK", number = 203, precision = 2)
    val danishKrone_DKK = Currency(name = "Danish Krone", code = "DKK", number = 208, precision = 2)
    val djiboutiFranc_DJF = Currency(name = "Djibouti Franc", code = "DJF", number = 262, precision = 0)
    val dominicanPeso_DOP = Currency(name = "Dominican Peso", code = "DOP", number = 214, precision = 2)
    val dutchGuilder_NLG = Currency(name = "Dutch Guilder", code = "NLG", number = 528, precision = 2)
    val eastCaribbeanDollar_XCD = Currency(name = "East Caribbean Dollar", code = "XCD", number = 951, precision = 2)
    val egyptianPound_EGP = Currency(name = "Egyptian Pound", code = "EGP", number = 818, precision = 2)
    val elSalvadorColon_SVC = Currency(name = "El Salvador Colon", code = "SVC", number = 222, precision = 2)
    val estonianKroon_EEK = Currency(name = "Estonian Kroon", code = "EEK", number = 233, precision = 2)
    val ethiopianBirr_ETB = Currency(name = "Ethiopian Birr", code = "ETB", number = 230, precision = 2)
    val euro_EUR = Currency(name = "Euro", code = "EUR", number = 978, precision = 2)
    val falklandIslandsPound_FKP = Currency(name = "Falkland Islands Pound", code = "FKP", number = 238, precision = 2)
    val fijiDollar_FJD = Currency(name = "Fiji Dollar", code = "FJD", number = 242, precision = 2)
    val finnishMarkka_FIM = Currency(name = "Finnish Markka", code = "FIM", number = 246, precision = 2)
    val frenchFranc_FRF = Currency(name = "French Franc", code = "FRF", number = 250, precision = 2)
    val gambianDalasi_GMD = Currency(name = "Gambian Dalasi", code = "GMD", number = 270, precision = 2)
    val georgianLari_GEL = Currency(name = "Georgian Lari", code = "GEL", number = 981, precision = 2)
    val germanMark_DEM = Currency(name = "German Mark", code = "DEM", number = 276, precision = 2)
    val ghanaCedi_GHS = Currency(name = "Ghana Cedi", code = "GHS", number = 936, precision = 2)
    val ghanaianCedi1979_2007_GHC = Currency(
        name = "Ghanaian Cedi (1979–2007)",
        code = "GHC",
        number = 288,
        precision = 2
    )
    val gibraltarPound_GIP = Currency(name = "Gibraltar Pound", code = "GIP", number = 292, precision = 2)
    val greekDrachma_GRD = Currency(name = "Greek Drachma", code = "GRD", number = 300, precision = 0)
    val guinea_bissauPeso_GWP = Currency(name = "Guinea-Bissau Peso", code = "GWP", number = 624, precision = 2)
    val guineanFranc_GNF = Currency(name = "Guinean Franc", code = "GNF", number = 324, precision = 0)
    val guyanaDollar_GYD = Currency(name = "Guyana Dollar", code = "GYD", number = 328, precision = 2)
    val haitianGourde_HTG = Currency(name = "Haitian Gourde", code = "HTG", number = 332, precision = 2)
    val honduranLempira_HNL = Currency(name = "Honduran Lempira", code = "HNL", number = 340, precision = 2)
    val hongKongDollar_HKD = Currency(name = "Hong Kong Dollar", code = "HKD", number = 344, precision = 2)
    val hungarianForint_HUF = Currency(name = "Hungarian Forint", code = "HUF", number = 348, precision = 2)
    val icelandKrona_ISK = Currency(name = "Iceland Krona", code = "ISK", number = 352, precision = 0)
    val indianRupee_INR = Currency(name = "Indian Rupee", code = "INR", number = 356, precision = 2)
    val iranianRial_IRR = Currency(name = "Iranian Rial", code = "IRR", number = 364, precision = 2)
    val iraqiDinar_IQD = Currency(name = "Iraqi Dinar", code = "IQD", number = 368, precision = 3)
    val irishPound_IEP = Currency(name = "Irish Pound", code = "IEP", number = 372, precision = 2)
    val italianLira_ITL = Currency(name = "Italian Lira", code = "ITL", number = 380, precision = 0)
    val jamaicanDollar_JMD = Currency(name = "Jamaican Dollar", code = "JMD", number = 388, precision = 2)
    val jordanianDinar_JOD = Currency(name = "Jordanian Dinar", code = "JOD", number = 400, precision = 3)
    val kenyanShilling_KES = Currency(name = "Kenyan Shilling", code = "KES", number = 404, precision = 2)
    val kuwaitiDinar_KWD = Currency(name = "Kuwaiti Dinar", code = "KWD", number = 414, precision = 3)
    val laoKip_LAK = Currency(name = "Lao Kip", code = "LAK", number = 418, precision = 2)
    val latvianLats_LVL = Currency(name = "Latvian Lats", code = "LVL", number = 428, precision = 2)
    val lebanesePound_LBP = Currency(name = "Lebanese Pound", code = "LBP", number = 422, precision = 2)
    val leone_SLE = Currency(name = "Leone", code = "SLE", number = 925, precision = 2)
    val leone_SLL = Currency(name = "Leone", code = "SLL", number = 694, precision = 2)
    val liberianDollar_LRD = Currency(name = "Liberian Dollar", code = "LRD", number = 430, precision = 2)
    val libyanDinar_LYD = Currency(name = "Libyan Dinar", code = "LYD", number = 434, precision = 3)
    val lilangeni_SZL = Currency(name = "Lilangeni", code = "SZL", number = 748, precision = 2)
    val lithuanianLitas_LTL = Currency(name = "Lithuanian Litas", code = "LTL", number = 440, precision = 2)
    val loti_LSL = Currency(name = "Loti", code = "LSL", number = 426, precision = 2)
    val luxembourgianFranc_LUF = Currency(name = "Luxembourgian Franc", code = "LUF", number = 442, precision = 0)
    val macedonianDenar_MKD = Currency(name = "Macedonian Denar", code = "MKD", number = 807, precision = 2)
    val malagasyAriary_MGA = Currency(name = "Malagasy Ariary", code = "MGA", number = 969, precision = 2)
    val malagasyFranc_MGF = Currency(name = "Malagasy Franc", code = "MGF", number = 450, precision = 0)
    val malawiKwacha_MWK = Currency(name = "Malawi Kwacha", code = "MWK", number = 454, precision = 2)
    val malaysianRinggit_MYR = Currency(name = "Malaysian Ringgit", code = "MYR", number = 458, precision = 2)
    val malteseLira_MTL = Currency(name = "Maltese Lira", code = "MTL", number = 470, precision = 2)
    val mauritanianOuguiya1973_2017_MRO = Currency(
        name = "Mauritanian Ouguiya (1973–2017)",
        code = "MRO",
        number = 478,
        precision = 2
    )
    val mauritiusRupee_MUR = Currency(name = "Mauritius Rupee", code = "MUR", number = 480, precision = 2)
    val mexicanPeso_MXN = Currency(name = "Mexican Peso", code = "MXN", number = 484, precision = 2)
    val mexicanUnidadDeInversionUdi_MXV = Currency(
        name = "Mexican Unidad de Inversion (UDI)",
        code = "MXV",
        number = 979,
        precision = 2
    )
    val moldovanLeu_MDL = Currency(name = "Moldovan Leu", code = "MDL", number = 498, precision = 2)
    val moroccanDirham_MAD = Currency(name = "Moroccan Dirham", code = "MAD", number = 504, precision = 2)
    val mozambicanMetical1980_2006_MZM = Currency(
        name = "Mozambican Metical (1980–2006)",
        code = "MZM",
        number = 508,
        precision = 2
    )
    val mozambiqueMetical_MZN = Currency(name = "Mozambique Metical", code = "MZN", number = 943, precision = 2)
    val mvdol_BOV = Currency(name = "Mvdol", code = "BOV", number = 984, precision = 2)
    val myanmarKyat_MMK = Currency(name = "Myanmar Kyat", code = "MMK", number = 104, precision = 2)
    val naira_NGN = Currency(name = "Naira", code = "NGN", number = 566, precision = 2)
    val nakfa_ERN = Currency(name = "Nakfa", code = "ERN", number = 232, precision = 2)
    val namibiaDollar_NAD = Currency(name = "Namibia Dollar", code = "NAD", number = 516, precision = 2)
    val nepaleseRupee_NPR = Currency(name = "Nepalese Rupee", code = "NPR", number = 524, precision = 2)
    val netherlandsAntilleanGuilder_ANG = Currency(
        name = "Netherlands Antillean Guilder",
        code = "ANG",
        number = 532,
        precision = 2
    )
    val newIsraeliSheqel_ILS = Currency(name = "New Israeli Sheqel", code = "ILS", number = 376, precision = 2)
    val newTaiwanDollar_TWD = Currency(name = "New Taiwan Dollar", code = "TWD", number = 901, precision = 2)
    val newZealandDollar_NZD = Currency(name = "New Zealand Dollar", code = "NZD", number = 554, precision = 2)
    val ngultrum_BTN = Currency(name = "Ngultrum", code = "BTN", number = 64, precision = 2)
    val nicaraguanCordoba_NIO = Currency(name = "Nicaraguan Córdoba", code = "NIO", number = 558, precision = 2)
    val northKoreanWon_KPW = Currency(name = "North Korean Won", code = "KPW", number = 408, precision = 2)
    val norwegianKrone_NOK = Currency(name = "Norwegian Krone", code = "NOK", number = 578, precision = 2)
    val ouguiya_MRU = Currency(name = "Ouguiya", code = "MRU", number = 929, precision = 2)
    val paAnga_TOP = Currency(name = "Pa’anga", code = "TOP", number = 776, precision = 2)
    val pakistanRupee_PKR = Currency(name = "Pakistan Rupee", code = "PKR", number = 586, precision = 2)
    val panamanianBalboa_PAB = Currency(name = "Panamanian Balboa", code = "PAB", number = 590, precision = 2)
    val papuaNewGuineanKina_PGK = Currency(name = "Papua New Guinean Kina", code = "PGK", number = 598, precision = 2)
    val paraguayanGuarani_PYG = Currency(name = "Paraguayan Guarani", code = "PYG", number = 600, precision = 0)
    val pataca_MOP = Currency(name = "Pataca", code = "MOP", number = 446, precision = 2)
    val pesoConvertible_CUC = Currency(name = "Peso Convertible", code = "CUC", number = 931, precision = 2)
    val pesoUruguayo_UYU = Currency(name = "Peso Uruguayo", code = "UYU", number = 858, precision = 2)
    val philippinePeso_PHP = Currency(name = "Philippine Peso", code = "PHP", number = 608, precision = 2)
    val portugueseEscudo_PTE = Currency(name = "Portuguese Escudo", code = "PTE", number = 620, precision = 0)
    val poundSterling_GBP = Currency(name = "Pound Sterling", code = "GBP", number = 826, precision = 2)
    val pula_BWP = Currency(name = "Pula", code = "BWP", number = 72, precision = 2)
    val qatariRial_QAR = Currency(name = "Qatari Rial", code = "QAR", number = 634, precision = 2)
    val quetzal_GTQ = Currency(name = "Quetzal", code = "GTQ", number = 320, precision = 2)
    val rand_ZAR = Currency(name = "Rand", code = "ZAR", number = 710, precision = 2)
    val rialOmani_OMR = Currency(name = "Rial Omani", code = "OMR", number = 512, precision = 3)
    val riel_KHR = Currency(name = "Riel", code = "KHR", number = 116, precision = 2)
    val romanianLeu1952_2006_ROL = Currency(
        name = "Romanian Leu (1952–2006)",
        code = "ROL",
        number = 642,
        precision = 0
    )
    val romanianLeu_RON = Currency(name = "Romanian Leu", code = "RON", number = 946, precision = 2)
    val rufiyaa_MVR = Currency(name = "Rufiyaa", code = "MVR", number = 462, precision = 2)
    val rupiah_IDR = Currency(name = "Rupiah", code = "IDR", number = 360, precision = 2)
    val russianRuble1991_1998_RUR = Currency(
        name = "Russian Ruble (1991–1998)",
        code = "RUR",
        number = 810,
        precision = 2
    )
    val russianRuble_RUB = Currency(name = "Russian Ruble", code = "RUB", number = 643, precision = 2)
    val rwandaFranc_RWF = Currency(name = "Rwanda Franc", code = "RWF", number = 646, precision = 0)
    val saintHelenaPound_SHP = Currency(name = "Saint Helena Pound", code = "SHP", number = 654, precision = 2)
    val saoTome_PrincipeDobra1977_2017_STD = Currency(
        name = "São Tomé & Príncipe Dobra (1977–2017)",
        code = "STD",
        number = 678,
        precision = 2
    )
    val saoTome_PrincipeDobra_STN = Currency(
        name = "São Tomé & Príncipe Dobra",
        code = "STN",
        number = 930,
        precision = 2
    )
    val saudiRiyal_SAR = Currency(name = "Saudi Riyal", code = "SAR", number = 682, precision = 2)
    val serbianDinar2002_2006_CSD = Currency(
        name = "Serbian Dinar (2002–2006)",
        code = "CSD",
        number = 891,
        precision = 2
    )
    val serbianDinar_RSD = Currency(name = "Serbian Dinar", code = "RSD", number = 941, precision = 2)
    val seychellesRupee_SCR = Currency(name = "Seychelles Rupee", code = "SCR", number = 690, precision = 2)
    val singaporeDollar_SGD = Currency(name = "Singapore Dollar", code = "SGD", number = 702, precision = 2)
    val slovakKoruna_SKK = Currency(name = "Slovak Koruna", code = "SKK", number = 703, precision = 2)
    val slovenianTolar_SIT = Currency(name = "Slovenian Tolar", code = "SIT", number = 705, precision = 2)
    val sol_PEN = Currency(name = "Sol", code = "PEN", number = 604, precision = 2)
    val solomonIslandsDollar_SBD = Currency(name = "Solomon Islands Dollar", code = "SBD", number = 90, precision = 2)
    val som_KGS = Currency(name = "Som", code = "KGS", number = 417, precision = 2)
    val somaliShilling_SOS = Currency(name = "Somali Shilling", code = "SOS", number = 706, precision = 2)
    val somoni_TJS = Currency(name = "Somoni", code = "TJS", number = 972, precision = 2)
    val southSudanesePound_SSP = Currency(name = "South Sudanese Pound", code = "SSP", number = 728, precision = 2)
    val spanishPeseta_ESP = Currency(name = "Spanish Peseta", code = "ESP", number = 724, precision = 0)
    val sriLankaRupee_LKR = Currency(name = "Sri Lanka Rupee", code = "LKR", number = 144, precision = 2)
    val sudaneseDinar1992_2007_SDD = Currency(
        name = "Sudanese Dinar (1992–2007)",
        code = "SDD",
        number = 736,
        precision = 2
    )
    val sudanesePound_SDG = Currency(name = "Sudanese Pound", code = "SDG", number = 938, precision = 2)
    val surinamDollar_SRD = Currency(name = "Surinam Dollar", code = "SRD", number = 968, precision = 2)
    val surinameseGuilder_SRG = Currency(name = "Surinamese Guilder", code = "SRG", number = 740, precision = 2)
    val swedishKrona_SEK = Currency(name = "Swedish Krona", code = "SEK", number = 752, precision = 2)
    val swissFranc_CHF = Currency(name = "Swiss Franc", code = "CHF", number = 756, precision = 2)
    val syrianPound_SYP = Currency(name = "Syrian Pound", code = "SYP", number = 760, precision = 2)
    val taka_BDT = Currency(name = "Taka", code = "BDT", number = 50, precision = 2)
    val tala_WST = Currency(name = "Tala", code = "WST", number = 882, precision = 2)
    val tanzanianShilling_TZS = Currency(name = "Tanzanian Shilling", code = "TZS", number = 834, precision = 2)
    val tenge_KZT = Currency(name = "Tenge", code = "KZT", number = 398, precision = 2)
    val thaiBaht_THB = Currency(name = "Thai Baht", code = "THB", number = 764, precision = 2)
    val timoreseEscudo_TPE = Currency(name = "Timorese Escudo", code = "TPE", number = 626, precision = 0)
    val trinidadAndTobagoDollar_TTD = Currency(
        name = "Trinidad and Tobago Dollar",
        code = "TTD",
        number = 780,
        precision = 2
    )
    val tugrik_MNT = Currency(name = "Tugrik", code = "MNT", number = 496, precision = 2)
    val tunisianDinar_TND = Currency(name = "Tunisian Dinar", code = "TND", number = 788, precision = 3)
    val turkishLira1922_2005_TRL = Currency(
        name = "Turkish Lira (1922–2005)",
        code = "TRL",
        number = 792,
        precision = 0
    )
    val turkishLira_TRY = Currency(name = "Turkish Lira", code = "TRY", number = 949, precision = 2)
    val turkmenistanNewManat_TMT = Currency(name = "Turkmenistan New Manat", code = "TMT", number = 934, precision = 2)
    val turkmenistaniManat1993_2009_TMM = Currency(
        name = "Turkmenistani Manat (1993–2009)",
        code = "TMM",
        number = 795,
        precision = 2
    )
    val uaeDirham_AED = Currency(name = "UAE Dirham", code = "AED", number = 784, precision = 2)
    val ugandaShilling_UGX = Currency(name = "Uganda Shilling", code = "UGX", number = 800, precision = 0)
    val ukrainianHryvnia_UAH = Currency(name = "Ukrainian Hryvnia", code = "UAH", number = 980, precision = 2)
    val unidadDeFomento_CLF = Currency(name = "Unidad de Fomento", code = "CLF", number = 990, precision = 4)
    val unidadDeValorReal_COU = Currency(name = "Unidad de Valor Real", code = "COU", number = 970, precision = 2)
    val unidadPrevisional_UYW = Currency(name = "Unidad Previsional", code = "UYW", number = 927, precision = 4)
    val uruguayPesoEnUnidadesIndexadasUi_UYI = Currency(
        name = "Uruguay Peso en Unidades Indexadas (UI)",
        code = "UYI",
        number = 940,
        precision = 0
    )
    val usDollarNextDay_USN = Currency(name = "US Dollar (Next day)", code = "USN", number = 997, precision = 2)
    val usDollarSameDay_USS = Currency(name = "US Dollar (Same day)", code = "USS", number = 998, precision = 2)
    val usDollar_USD = Currency(name = "US Dollar", code = "USD", number = 840, precision = 2)
    val uzbekistanSum_UZS = Currency(name = "Uzbekistan Sum", code = "UZS", number = 860, precision = 2)
    val vatu_VUV = Currency(name = "Vatu", code = "VUV", number = 548, precision = 0)
    val venezuelanBolivar1871_2008_VEB = Currency(
        name = "Venezuelan Bolívar (1871–2008)",
        code = "VEB",
        number = 862,
        precision = 2
    )
    val venezuelanBolivarSoberano_VED = Currency(
        name = "Venezuelan Bolívar Soberano",
        code = "VED",
        number = 926,
        precision = 2
    )
    val venezuelanBolivarSoberano_VES = Currency(
        name = "Venezuelan Bolívar Soberano",
        code = "VES",
        number = 928,
        precision = 2
    )
    val venezuelanBolivar_VEF = Currency(name = "Venezuelan Bolívar", code = "VEF", number = 937, precision = 2)
    val vietnameseDong_VND = Currency(name = "Vietnamese Dong", code = "VND", number = 704, precision = 0)
    val westAfricanCfaFranc_XOF = Currency(name = "West African CFA Franc", code = "XOF", number = 952, precision = 0)
    val wirEuro_CHE = Currency(name = "WIR Euro", code = "CHE", number = 947, precision = 2)
    val wirFranc_CHW = Currency(name = "WIR Franc", code = "CHW", number = 948, precision = 2)
    val won_KRW = Currency(name = "Won", code = "KRW", number = 410, precision = 0)
    val yemeniRial_YER = Currency(name = "Yemeni Rial", code = "YER", number = 886, precision = 2)
    val yen_JPY = Currency(name = "Yen", code = "JPY", number = 392, precision = 0)
    val yuanRenminbi_CNY = Currency(name = "Yuan Renminbi", code = "CNY", number = 156, precision = 2)
    val yugoslavianNewDinar1994_2002_YUM = Currency(
        name = "Yugoslavian New Dinar (1994–2002)",
        code = "YUM",
        number = 891,
        precision = 2
    )
    val zambianKwacha1968_2012_ZMK = Currency(
        name = "Zambian Kwacha (1968–2012)",
        code = "ZMK",
        number = 894,
        precision = 2
    )
    val zambianKwacha_ZMW = Currency(name = "Zambian Kwacha", code = "ZMW", number = 967, precision = 2)
    val zimbabweDollar_ZWL = Currency(name = "Zimbabwe Dollar", code = "ZWL", number = 932, precision = 2)
    val zimbabweanDollar1980_2008_ZWD = Currency(
        name = "Zimbabwean Dollar (1980–2008)",
        code = "ZWD",
        number = 716,
        precision = 2
    )
    val zimbabweanDollar2008_ZWR = Currency(
        name = "Zimbabwean Dollar (2008)",
        code = "ZWR",
        number = 935,
        precision = 2
    )
    val zloty_PLN = Currency(name = "Zloty", code = "PLN", number = 985, precision = 2)
    val zwn_ZWN = Currency(name = "ZWN", code = "ZWN", number = 942, precision = 2)

    val all = listOf(
        afghanAfghani1927_2002_AFA,
        afghanAfghani_AFN,
        albanianLek_ALL,
        algerianDinar_DZD,
        andorranPeseta_ADP,
        angolanKwanza_AOA,
        argentinePeso_ARS,
        armenianDram_AMD,
        arubanFlorin_AWG,
        australianDollar_AUD,
        austrianSchilling_ATS,
        aym_AYM,
        azerbaijanManat_AZN,
        azerbaijaniManat1993_2006_AZM,
        bahamianDollar_BSD,
        bahrainiDinar_BHD,
        barbadianDollar_BBD,
        belarusianRuble1994_1999_BYB,
        belarusianRuble2000_2016_BYR,
        belarusianRuble_BYN,
        belgianFranc_BEF,
        belizeDollar_BZD,
        bermudianDollar_BMD,
        bolivianBoliviano_BOB,
        bosnia_herzegovinaConvertibleMark_BAM,
        brazilianReal_BRL,
        bruneiDollar_BND,
        bulgarianHardLev_BGL,
        bulgarianLev_BGN,
        burundiFranc_BIF,
        caboVerdeEscudo_CVE,
        canadianDollar_CAD,
        caymanIslandsDollar_KYD,
        centralAfricanCfaFranc_XAF,
        cfpFranc_XPF,
        chileanPeso_CLP,
        colombianPeso_COP,
        comorianFranc_KMF,
        congoleseFranc_CDF,
        costaRicanColon_CRC,
        croatianKuna_HRK,
        cubanPeso_CUP,
        cypriotPound_CYP,
        czechKoruna_CZK,
        danishKrone_DKK,
        djiboutiFranc_DJF,
        dominicanPeso_DOP,
        dutchGuilder_NLG,
        eastCaribbeanDollar_XCD,
        egyptianPound_EGP,
        elSalvadorColon_SVC,
        estonianKroon_EEK,
        ethiopianBirr_ETB,
        euro_EUR,
        falklandIslandsPound_FKP,
        fijiDollar_FJD,
        finnishMarkka_FIM,
        frenchFranc_FRF,
        gambianDalasi_GMD,
        georgianLari_GEL,
        germanMark_DEM,
        ghanaCedi_GHS,
        ghanaianCedi1979_2007_GHC,
        gibraltarPound_GIP,
        greekDrachma_GRD,
        guinea_bissauPeso_GWP,
        guineanFranc_GNF,
        guyanaDollar_GYD,
        haitianGourde_HTG,
        honduranLempira_HNL,
        hongKongDollar_HKD,
        hungarianForint_HUF,
        icelandKrona_ISK,
        indianRupee_INR,
        iranianRial_IRR,
        iraqiDinar_IQD,
        irishPound_IEP,
        italianLira_ITL,
        jamaicanDollar_JMD,
        jordanianDinar_JOD,
        kenyanShilling_KES,
        kuwaitiDinar_KWD,
        laoKip_LAK,
        latvianLats_LVL,
        lebanesePound_LBP,
        leone_SLE,
        leone_SLL,
        liberianDollar_LRD,
        libyanDinar_LYD,
        lilangeni_SZL,
        lithuanianLitas_LTL,
        loti_LSL,
        luxembourgianFranc_LUF,
        macedonianDenar_MKD,
        malagasyAriary_MGA,
        malagasyFranc_MGF,
        malawiKwacha_MWK,
        malaysianRinggit_MYR,
        malteseLira_MTL,
        mauritanianOuguiya1973_2017_MRO,
        mauritiusRupee_MUR,
        mexicanPeso_MXN,
        mexicanUnidadDeInversionUdi_MXV,
        moldovanLeu_MDL,
        moroccanDirham_MAD,
        mozambicanMetical1980_2006_MZM,
        mozambiqueMetical_MZN,
        mvdol_BOV,
        myanmarKyat_MMK,
        naira_NGN,
        nakfa_ERN,
        namibiaDollar_NAD,
        nepaleseRupee_NPR,
        netherlandsAntilleanGuilder_ANG,
        newIsraeliSheqel_ILS,
        newTaiwanDollar_TWD,
        newZealandDollar_NZD,
        ngultrum_BTN,
        nicaraguanCordoba_NIO,
        northKoreanWon_KPW,
        norwegianKrone_NOK,
        ouguiya_MRU,
        paAnga_TOP,
        pakistanRupee_PKR,
        panamanianBalboa_PAB,
        papuaNewGuineanKina_PGK,
        paraguayanGuarani_PYG,
        pataca_MOP,
        pesoConvertible_CUC,
        pesoUruguayo_UYU,
        philippinePeso_PHP,
        portugueseEscudo_PTE,
        poundSterling_GBP,
        pula_BWP,
        qatariRial_QAR,
        quetzal_GTQ,
        rand_ZAR,
        rialOmani_OMR,
        riel_KHR,
        romanianLeu1952_2006_ROL,
        romanianLeu_RON,
        rufiyaa_MVR,
        rupiah_IDR,
        russianRuble1991_1998_RUR,
        russianRuble_RUB,
        rwandaFranc_RWF,
        saintHelenaPound_SHP,
        saoTome_PrincipeDobra1977_2017_STD,
        saoTome_PrincipeDobra_STN,
        saudiRiyal_SAR,
        serbianDinar2002_2006_CSD,
        serbianDinar_RSD,
        seychellesRupee_SCR,
        singaporeDollar_SGD,
        slovakKoruna_SKK,
        slovenianTolar_SIT,
        sol_PEN,
        solomonIslandsDollar_SBD,
        som_KGS,
        somaliShilling_SOS,
        somoni_TJS,
        southSudanesePound_SSP,
        spanishPeseta_ESP,
        sriLankaRupee_LKR,
        sudaneseDinar1992_2007_SDD,
        sudanesePound_SDG,
        surinamDollar_SRD,
        surinameseGuilder_SRG,
        swedishKrona_SEK,
        swissFranc_CHF,
        syrianPound_SYP,
        taka_BDT,
        tala_WST,
        tanzanianShilling_TZS,
        tenge_KZT,
        thaiBaht_THB,
        timoreseEscudo_TPE,
        trinidadAndTobagoDollar_TTD,
        tugrik_MNT,
        tunisianDinar_TND,
        turkishLira1922_2005_TRL,
        turkishLira_TRY,
        turkmenistanNewManat_TMT,
        turkmenistaniManat1993_2009_TMM,
        uaeDirham_AED,
        ugandaShilling_UGX,
        ukrainianHryvnia_UAH,
        unidadDeFomento_CLF,
        unidadDeValorReal_COU,
        unidadPrevisional_UYW,
        uruguayPesoEnUnidadesIndexadasUi_UYI,
        usDollarNextDay_USN,
        usDollarSameDay_USS,
        usDollar_USD,
        uzbekistanSum_UZS,
        vatu_VUV,
        venezuelanBolivar1871_2008_VEB,
        venezuelanBolivarSoberano_VED,
        venezuelanBolivarSoberano_VES,
        venezuelanBolivar_VEF,
        vietnameseDong_VND,
        westAfricanCfaFranc_XOF,
        wirEuro_CHE,
        wirFranc_CHW,
        won_KRW,
        yemeniRial_YER,
        yen_JPY,
        yuanRenminbi_CNY,
        yugoslavianNewDinar1994_2002_YUM,
        zambianKwacha1968_2012_ZMK,
        zambianKwacha_ZMW,
        zimbabweDollar_ZWL,
        zimbabweanDollar1980_2008_ZWD,
        zimbabweanDollar2008_ZWR,
        zloty_PLN,
        zwn_ZWN
    )
    val byCode = all.associateBy { it.code }
}
