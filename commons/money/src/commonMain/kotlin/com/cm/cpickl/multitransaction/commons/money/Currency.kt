package com.cm.cpickl.multitransaction.commons.money

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = CurrencySerializer::class)
data class Currency
    (
    /** Human readable display name, e.g.: "Danish Krone" */
    // TODO JVM currency supports localized version; could do so as well
    val name: String,
    /** 3 uppercase letter, e.g.: "DKK" */
    val code: String,
    /** Internal code, e.g.: 208 */
    val number: Int,
    /** Number of decimals, e.g. mostly: 2 */
    val precision: Int,

    // TODO some currencies have symbols (JVM currency has them); could do so as well
) {
    companion object {
        /**
         * @param code 3 uppercase letter of currency code.
         */
        fun byCodeOrNull(code: String): Currency? =
            Currencies.byCode[code]

        /**
         * @param code 3 uppercase letter of currency code.
         */
        fun byCodeOrThrow(code: String): Currency =
            byCodeOrNull(code) ?: error("Unsupported currency code: '$code'!")

        fun random() = Currencies.all.random()
    }

    init {
        require(code.matches(Regex("""^[A-Z]{3}$""")))
        require(precision >= 0) { "Precision must be >= 0 but was $precision (code=$code)" }
        require(number >= 0) { "Number must be >= 0 but was $number (code=$code)" }
    }
}

object CurrencySerializer : KSerializer<Currency> {
    override val descriptor = PrimitiveSerialDescriptor(
        "com.cm.cpickl.multitransaction.commons.money.Currency",
        PrimitiveKind.STRING
    )

    override fun serialize(encoder: Encoder, value: Currency) {
        encoder.encodeString(value.code)
    }

    override fun deserialize(decoder: Decoder) =
        Currency.byCodeOrThrow(decoder.decodeString())
}
