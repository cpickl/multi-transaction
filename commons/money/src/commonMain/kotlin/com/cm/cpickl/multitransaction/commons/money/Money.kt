package com.cm.cpickl.multitransaction.commons.money

import kotlinx.serialization.Serializable

@Serializable
data class Money(
    val value: Long,
    val currency: Currency,
) {
    companion object {
        fun euroCents(cents: Long) = Money(cents, Currencies.euro_EUR)
        fun euro(euro: Long) = euroCents(euro * 100)
    }

    private val readableString by lazy {
        var big = 1
        for (i in 1..currency.precision) {
            big *= 10
        }
        val numberPart = if (currency.precision == 0) {
            value.toString()
        } else if (value < big) {
            "0." + "0".repeat(currency.precision - value.toString().length) + value.toString()
        } else {
            (value.toDouble() / big).toLong().toString() + "." + (value % big).toString()
                .padStart(currency.precision, '0')
        }
        "${currency.code} $numberPart"
    }

    fun toReadableString() = readableString
}
