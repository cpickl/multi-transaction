package com.cm.cpickl.multitransaction.commons.money

import io.kotest.assertions.json.shouldEqualJson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class SerializationTest : StringSpec({
    "When encode currency Then return quoted code" {
        Json.encodeToString(Currencies.euro_EUR) shouldBe "\"EUR\""
    }
    "When encode money Then return JSON with code" {
        Json.encodeToString(Money(1, Currencies.euro_EUR)) shouldEqualJson """{ "value": 1, "currency": "EUR" }"""
    }
})

// TODO Arb.money.???
