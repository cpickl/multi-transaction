package com.cm.cpickl.multitransaction.commons.money

import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class MoneyTest : StringSpec({

    val currencyPrecision0 = Currencies.guineanFranc_GNF.also { require(it.precision == 0) }
    val currencyPrecision2 = Currencies.euro_EUR.also { require(it.precision == 2) }
    val currencyPrecision3 = Currencies.iraqiDinar_IQD.also { require(it.precision == 3) }

    "Money toReadableString" {
        assertSoftly {
            Money(0, currencyPrecision0).toReadableString() shouldBe "${currencyPrecision0.code} 0"
            Money(9, currencyPrecision0).toReadableString() shouldBe "${currencyPrecision0.code} 9"
            Money(10, currencyPrecision0).toReadableString() shouldBe "${currencyPrecision0.code} 10"
            Money(123, currencyPrecision0).toReadableString() shouldBe "${currencyPrecision0.code} 123"

            Money(0, currencyPrecision2).toReadableString() shouldBe "${currencyPrecision2.code} 0.00"
            Money(99, currencyPrecision2).toReadableString() shouldBe "${currencyPrecision2.code} 0.99"
            Money(100, currencyPrecision2).toReadableString() shouldBe "${currencyPrecision2.code} 1.00"
            Money(1234, currencyPrecision2).toReadableString() shouldBe "${currencyPrecision2.code} 12.34"

            Money(0, currencyPrecision3).toReadableString() shouldBe "${currencyPrecision3.code} 0.000"
            Money(999, currencyPrecision3).toReadableString() shouldBe "${currencyPrecision3.code} 0.999"
            Money(1000, currencyPrecision3).toReadableString() shouldBe "${currencyPrecision3.code} 1.000"
            Money(12345, currencyPrecision3).toReadableString() shouldBe "${currencyPrecision3.code} 12.345"
        }
    }
})