package com.cm.cpickl.multitransaction.commons.money

import java.util.Currency as JvmCurrency

fun Currency.toJvmCurrency(): JvmCurrency =
    try {
        JvmCurrency.getInstance(code)
    } catch (e: Exception) {
        throw Exception("Failed to convert $this to JVM currency!", e)
    }

fun JvmCurrency.toMppCurrency(): Currency =
    Currency.byCodeOrThrow(currencyCode)
