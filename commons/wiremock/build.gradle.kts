plugins {
    kotlin("jvm")
}

dependencies {
    api(Deps.Wiremock)
    api(Deps.Kotest.FrameworkApi)
    implementation(Deps.Serialization)
}
