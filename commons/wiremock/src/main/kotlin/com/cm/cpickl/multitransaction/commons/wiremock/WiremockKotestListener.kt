package com.cm.cpickl.multitransaction.commons.wiremock

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder
import com.github.tomakehurst.wiremock.verification.LoggedRequest
import io.kotest.core.listeners.AfterEachListener
import io.kotest.core.listeners.AfterSpecListener
import io.kotest.core.listeners.BeforeSpecListener
import io.kotest.core.spec.Spec
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult

class WiremockKotestListener(
    val port: Int = 8099
) : BeforeSpecListener, AfterSpecListener, AfterEachListener {

    private val server = WireMockServer(WireMockConfiguration.options().port(port))
    private val executor = WiremockExecutor(server)

    override suspend fun beforeSpec(spec: Spec) {
        server.start()
    }

    override suspend fun afterSpec(spec: Spec) {
        server.stop()
    }

    override suspend fun afterEach(testCase: TestCase, result: TestResult) {
        server.resetAll()
    }

    fun allRecordedRequests(): List<LoggedRequest> {
        return server.findAll(RequestPatternBuilder.allRequests())
    }

    fun stub(code: WiremockDsl.() -> Unit) =
        executor.execute(newStub(code))

}

