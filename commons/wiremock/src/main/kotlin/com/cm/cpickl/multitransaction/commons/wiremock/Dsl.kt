package com.cm.cpickl.multitransaction.commons.wiremock

@DslMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.TYPE)
annotation class WiremockDslMarker

fun newStub(code: WiremockDsl.() -> Unit): WiremockStub {
    val dsl = WiremockDslImpl()
    dsl.code()
    return dsl.buildStub()
}

@WiremockDslMarker
interface WiremockDsl {
    fun request(code: WiremockRequestDsl.() -> Unit)
    fun response(code: WiremockResponseDsl.() -> Unit)
}

@WiremockDslMarker
interface WiremockRequestDsl {
    var method: HttpMethod
    var url: String
    var headers: MutableMap<String, String>
}

@WiremockDslMarker
interface WiremockResponseDsl {
    var headers: MutableMap<String, String>
    var status: Int
    var body: Any?
}

internal class WiremockDslImpl : WiremockDsl {

    companion object {
        private const val statusNotSet = 0
    }

    private var request: WiremockRequest? = null
    private var response: WiremockResponse? = null

    fun buildStub() = WiremockStub(
        request = request ?: error("Request must be set!"),
        response = response ?: error("Response must be set!"),
    )

    override fun request(code: WiremockRequestDsl.() -> Unit) {
        val dsl = object : WiremockRequestDsl {
            override lateinit var method: HttpMethod
            override lateinit var url: String
            override var headers: MutableMap<String, String> = mutableMapOf()
        }
        dsl.code()
        request = WiremockRequest(
            method = dsl.method,
            url = dsl.url,
            headers = dsl.headers,
        )
    }

    override fun response(code: WiremockResponseDsl.() -> Unit) {
        val dsl = object : WiremockResponseDsl {
            override var status: Int = statusNotSet
            override var body: Any? = null
            override var headers: MutableMap<String, String> = mutableMapOf()
        }
        dsl.code()
        require(dsl.status != statusNotSet)
        response = WiremockResponse(
            status = dsl.status,
            body = dsl.body,
            headers = dsl.headers,
        )
    }
}

fun WiremockRequestDsl.jsonContentHeader() = apply {
    headers += "Content-Type" to "application/json"
}

fun WiremockResponseDsl.jsonContentHeader() = apply {
    headers += "Content-Type" to "application/json"
}
