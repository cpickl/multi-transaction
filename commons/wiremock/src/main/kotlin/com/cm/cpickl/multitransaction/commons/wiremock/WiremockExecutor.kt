package com.cm.cpickl.multitransaction.commons.wiremock

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.MappingBuilder
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class WiremockExecutor(
    private val wiremock: WireMockServer
) {
    fun execute(stub: WiremockStub) {
        val requestMapping = stub.buildRequestMapping()
        val responseDefinition = stub.buildResponseDefinition()
        requestMapping.willReturn(responseDefinition)
        wiremock.stubFor(requestMapping)
    }
}

private fun WiremockStub.buildRequestMapping(): MappingBuilder {
    val mapping = request.mapping
    request.headers.forEach {
        mapping.withHeader(it.key, WireMock.equalTo(it.value))
    }
    return mapping
}

private fun WiremockStub.buildResponseDefinition(): ResponseDefinitionBuilder {
    val responseDefinition = aResponse()
    responseDefinition.withStatus(response.status)
    response.headers.forEach {
        responseDefinition.withHeader(it.key, it.value)
    }
    responseDefinition.withBody(response.bodyAsString)
    return responseDefinition
}

private fun interface MethodToMapping {
    operator fun invoke(url: String): MappingBuilder
}

private val WiremockResponse.bodyAsString: String
    get() =
        if (body is String) body else Json.encodeToString(body)

private val HttpMethod.toMapping: MethodToMapping
    get() = when (this) {
        HttpMethod.Get -> MethodToMapping { url -> WireMock.get(url) }
        HttpMethod.Post -> MethodToMapping { url -> WireMock.post(url) }
    }

private val WiremockRequest.mapping: MappingBuilder
    get() =
        method.toMapping(url)
