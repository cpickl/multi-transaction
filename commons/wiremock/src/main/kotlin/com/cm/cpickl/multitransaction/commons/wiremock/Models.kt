package com.cm.cpickl.multitransaction.commons.wiremock

typealias Headers = Map<String, String>

data class WiremockStub(
    val request: WiremockRequest,
    val response: WiremockResponse,
)

data class WiremockRequest(
    val method: HttpMethod,
    val url: String,
    val headers: Headers = emptyMap(),
)

data class WiremockResponse(
    val status: Int,
    val body: Any?,
    val headers: Headers = emptyMap(),
)

enum class HttpMethod {
    Get,
    Post,
}
