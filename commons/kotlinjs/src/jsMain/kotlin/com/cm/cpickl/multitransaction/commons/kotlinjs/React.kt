package com.cm.cpickl.multitransaction.commons.kotlinjs

private val cachedCurrentEnvironment by lazy {
    val rawValue = (js("process.env.NODE_ENV") as? String)
        ?: throw Exception("Could not determine environment by evaluating 'process.env.NODE_ENV' as it returned null!")
    ReactEnvironment.findByStringValue(rawValue)
        ?: throw Exception("Unhandled environment string '$rawValue'!")
}

val currentEnvironment: ReactEnvironment get() = cachedCurrentEnvironment

enum class ReactEnvironment(private val stringValue: String) {
    Development("development"),
    Production("production");

    companion object {
        fun findByStringValue(search: String) = values().firstOrNull { it.stringValue == search }
    }
}
