#!/bin/sh

gcc -Wall -g \
  src/nativeclient.c \
  -o build/nativeclient \
  ../mykotlinlib/build/bin/native/debugShared/libnative.dylib \
  -I../mykotlinlib/build/bin/native/debugShared \
  || exit 1;

echo "Built successfully executable to build/nativeclient ✅";
