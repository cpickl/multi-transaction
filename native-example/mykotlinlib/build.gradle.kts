plugins {
    kotlin("multiplatform") version "1.7.20"
}


kotlin {
    // linuxX64("native") { // on Linux
    // macosArm64("native") { // on Apple Silicon macOS
    // mingwX64("native") { // on Windows
    macosX64("native") { // on x86_64 macOS
        binaries {
            sharedLib {
                baseName = "native" // on Linux and macOS
                // baseName = "libnative" // on Windows
            }
        }
    }
}


// ./gradlew linkNative
// see: build/bin/native/debugShared (libnative_api.h and libnative.dylib)